#!/bin/bash
#
# Обновить статику из билда верстки
#

rsync -av src/public/build/css/ src/public/css/
rsync -av src/public/build/scripts/ src/public/scripts/
rsync -av src/public/build/img/ src/public/img/
