#!/bin/bash

docker exec -it ideagrande_app rsync -av /var/www/html/public/build/css/ /var/www/html/public/css/
docker exec -it ideagrande_app rsync -av --delete /var/www/html/public/build/scripts/ /var/www/html/public/scripts/
docker exec -it ideagrande_app rsync -av --delete /var/www/html/public/build/img/ /var/www/html/public/img/
