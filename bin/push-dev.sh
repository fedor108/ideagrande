#!/bin/bash

git push origin master

ssh amolytics "cd /var/www/ideagrande.neq4.ru; git pull origin master; docker exec ideagrande_app composer install; docker exec ideagrande_app php artisan migrate"