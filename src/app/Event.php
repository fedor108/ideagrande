<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'title_ru',
        'text',
        'text_ru',
        'link',
        'active',
        'sort',
    ];

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable')->orderBy('sort', 'asc');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function addImages($files)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/events/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(['src' => $src]);
        }
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }
}
