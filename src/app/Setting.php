<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['title', 'name', 'value'];

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function addImages($files)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/settings/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(compact('src'));
        }
    }

    public function getObjAttribute()
    {
        return json_decode($this->attributes['value']);
    }

    public function getExploded($sep = ',')
    {
        return array_map('trim', explode($sep, $this->attributes['value']));
    }
}
