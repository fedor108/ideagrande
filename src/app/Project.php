<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    protected $fillable = [
        'title',
        'name',
        'description',
        'date',
        'task',
        'decision',
        'result',
        'lead',
        'video',
        'link',
        'active',
        'sort',
        'title_ru',
        'description_ru',
        'task_ru',
        'decision_ru',
        'result_ru',
        'lead_ru',
        'video_ru',
        'people',
        'coverage',
        'seo_title',
        'seo_title_ru',
    ];

    protected static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            if (empty($model->name)) {
                $model->name = $model->id;
                $model->save();
            }
        });

        self::updated(function ($model) {
            if (empty($model->name)) {
                $model->name = $model->id;
                $model->save();
            }
        });
    }

    public function clients()
    {
        return $this->morphToMany('App\Client', 'clientable')->orderBy('sort', 'asc');
    }

    public function services()
    {
        return $this->morphToMany('App\Service', 'serviceable')->orderBy('sort', 'asc');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable')->orderBy('sort', 'asc');
    }

    public function addImages($files, $data = null)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/projects/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(compact('src', 'data'));
        }
    }

    public function getImageAttribute()
    {
        return $this->images->first(function ($image) {
            return (isset($image->data_obj->type)
                && ('image' == $image->data_obj->type));
        });
    }

    public function getGalleryAttribute()
    {
        return $this->images->filter(function ($image) {
            return (isset($image->data_obj->type)
                && ('gallery' == $image->data_obj->type));
        });
    }

    public function getClippingsAttribute()
    {
        return $this->images->filter(function ($image) {
            return (isset($image->data_obj->type)
                && ('clippings' == $image->data_obj->type));
        });
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getSeoTitleAttribute()
    {
        return empty($this->attributes['seo_title']) ? $this->attributes['title'] : $this->attributes['seo_title'];
    }

    public function getSeoTitleRuAttribute()
    {
        return empty($this->attributes['seo_title_ru']) ? $this->attributes['title_ru'] : $this->attributes['seo_title'];
    }

    public function getLinkObjAttribute()
    {
        $tmp = explode(' ', $this->link);
        $url = $tmp[0];
        $text = empty($tmp[1]) ? $tmp[0] : $tmp[1];
        return json_decode(json_encode(compact('url', 'text')));
    }

}
