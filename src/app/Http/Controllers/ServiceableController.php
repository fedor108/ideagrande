<?php

namespace App\Http\Controllers;

use App\Serviceable;
use Illuminate\Http\Request;

class ServiceableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serviceable = new Serviceable;
        $serviceable->fill($request->all());
        $serviceable->save();

        return back()->with('store-serviceable-success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Serviceable  $serviceable
     * @return \Illuminate\Http\Response
     */
    public function show(Serviceable $serviceable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Serviceable  $serviceable
     * @return \Illuminate\Http\Response
     */
    public function edit(Serviceable $serviceable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Serviceable  $serviceable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Serviceable $serviceable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Serviceable  $serviceable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Serviceable $serviceable)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach(['serviceable_type', 'serviceable_id', 'service_id'] as $key) {
            if (empty($request->input($key))) {
                throw new Exception("Not enaugh data for delete serviceable: {$key}", 1);
            } else {
                $data[$key] = $request->input($key);
            }
        }

        $serviceable = Serviceable::where($data)->firstOrFail();

        $serviceable->delete();

        return back()->with('delete-serviceable-success', true);
    }
}
