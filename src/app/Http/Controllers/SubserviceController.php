<?php

namespace App\Http\Controllers;

use App\Subservice;
use Illuminate\Http\Request;

class SubserviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subservice = new Subservice;
        $subservice->fill($request->all());
        $subservice->save();

        return back()->with('store-subservice-success', $subservice->title);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Subservice  $subservice
     * @return \Illuminate\Http\Response
     */
    public function show(Subservice $subservice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subservice  $subservice
     * @return \Illuminate\Http\Response
     */
    public function edit(Subservice $subservice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subservice  $subservice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subservice  $subservice)
    {
        $subservice->fill($request->all());
        $subservice->active = empty($request->input('active')) ? 0 : 1;
        $subservice->save();

        return back()->with('update-subservice-success', $subservice->title);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subservice  $subservice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subservice $subservice)
    {
        $subservice->delete();

        return back()->with('destroy-subservice-success', $subservice->title);
    }

    public function sort(Request $request)
    {
        $sort = $request->input('sort');

        Subservice::whereIn('id', $sort)->get()->each(function ($subservice) use ($sort) {
            $subservice->sort = array_search($subservice->id, $sort);
            $subservice->save();
        });

        return json_encode(compact('sort'));
    }
}
