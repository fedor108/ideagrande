<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = new Project;
        $project->fill($request->all());
        $project->save();

        return back()->with('store-project-success', $project->title);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        if (! empty($request->file('image-uploads'))) {
            $project->addImages($request->file('image-uploads'), ['type' => 'image']);
        }

        if (! empty($request->file('gallery-uploads'))) {
            $project->addImages($request->file('gallery-uploads'), ['type' => 'gallery']);
        }

        if (! empty($request->file('clippings-uploads' ))) {
            $project->addImages($request->file('clippings-uploads'), ['type' => 'clippings']);
        }

        $project->fill($request->all());
        $project->active = empty($request->input('active')) ? 0 : 1;
        $project->save();


        return back()->with('update-project-success', $project->title);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();

        return back()->with('destroy-project-success', $project->title);
    }

    public function sort(Request $request)
    {
        $sort = $request->input('sort');

        Project::whereIn('id', $sort)->get()->each(function ($project) use ($sort) {
            $project->sort = array_search($project->id, $sort);
            $project->save();
        });

        return json_encode(compact('sort'));
    }

}
