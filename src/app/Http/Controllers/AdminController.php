<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Subservice;
use App\Client;
use App\Practice;
use App\Person;
use App\Page;
use App\Image;
use App\Setting;
use App\User;
use App\Project;
use App\Vacancy;
use App\Event;
use App\News;
use App\Tag;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return redirect()->action('AdminController@pages');
    }

    public function services()
    {
        $services = Service::ordered()->get();

        $page = Page::where('name', 'services')->first();

        return view('admin.services', compact('services', 'page'));
    }

    public function service($id)
    {
        $service = Service::findOrFail($id);

        return view('admin.service', compact('service'));
    }

    public function subservice($id)
    {
        $subservice = Subservice::findOrFail($id);

        return view('admin.subservice', compact('subservice'));
    }

    public function clients()
    {
        $clients = Client::ordered()->get();

        return view('admin.clients', compact('clients'));
    }

    public function client($id)
    {
        $client = Client::find($id);

        return view('admin.client', compact('client'));
    }

    public function practices()
    {
        $practices = Practice::ordered()->get();

        $page = Page::where('name', 'practices')->first();

        return view('admin.practices', compact('practices', 'page'));
    }

    public function practice($id)
    {
        $practice = Practice::find($id);

        $clients = Client::active()->get()->filter(function ($client) use ($practice) {
            return (! in_array($client->id, $practice->clients->pluck('id')->toArray()));
        });

        return view('admin.practice', compact('practice', 'clients'));
    }

    public function persons()
    {
        $persons = Person::ordered()->get();

        $page = Page::where('name', 'about')->first();

        return view('admin.persons', compact('persons', 'page'));
    }

    public function person($id)
    {
        $person = Person::find($id);

        return view('admin.person', compact('person'));
    }

    public function pages()
    {
        $pages = Page::ordered()->get();

        return view('admin.pages', compact('pages'));
    }

    public function page($id)
    {
        $page = Page::find($id);

        if (in_array($page->name, ['services', 'practices'])) {
            $action  = 'AdminController@' . $page->name;
            return redirect()->action($action);
        } elseif ('about' == $page->name) {
            return redirect()->action('AdminController@persons');
        } elseif ('cases' == $page->name) {
            return redirect()->action('AdminController@projects');
        } elseif ('hiring' == $page->name) {
            return redirect()->action('AdminController@vacancies');
        } elseif ('news' == $page->name) {
            return redirect()->action('AdminController@news');
        }

        return view('admin.page', compact('page'));
    }

    public function image($id)
    {
        $image = Image::find($id);

        return view('admin.image', compact('image'));
    }

    public function imageable($id)
    {
        $image = Image::find($id);

        $tmp = explode('\\', $image->imageable_type);

        $action = 'AdminController@' . strtolower(end($tmp));

        return redirect()->action($action, ['id' => $image->imageable_id]);
    }


    public function settings()
    {
        $settings = Setting::orderBy('title')->get();

        return view('admin.settings', compact('settings'));
    }

    public function setting($id)
    {
        $setting = Setting::find($id);

        return view('admin.setting', compact('setting'));
    }

    public function users()
    {
        $users = User::all();

        return view('admin.users', compact('users'));
    }

    public function user($id)
    {
        $user = User::find($id);

        return view('admin.user', compact('user'));
    }

    public function projects()
    {
        $projects = Project::ordered()->get();

        $page = Page::where('name', 'cases')->first();

        return view('admin.projects', compact('projects', 'page'));
    }

    public function project($id)
    {
        $project = Project::findOrFail($id);

        $clients = Client::active()->get()->filter(function ($client) use ($project) {
            return (! in_array($client->id, $project->clients->pluck('id')->toArray()));
        });

        $services = Service::active()->get()->filter(function ($servise) use ($project) {
            return (! in_array($servise->id, $project->services->pluck('id')->toArray()));
        });

        return view('admin.project', compact('project', 'clients', 'services'));
    }

    public function vacancies()
    {
        $vacancies = Vacancy::ordered()->get();

        $page = Page::where('name', 'hiring')->first();

        return view('admin.vacancies', compact('vacancies', 'page'));
    }

    public function vacancy($id)
    {
        $vacancy = Vacancy::find($id);

        return view('admin.vacancy', compact('vacancy'));
    }


    public function events()
    {
        $events = Event::ordered()->get();

        return view('admin.events', compact('events'));
    }

    public function event($id)
    {
        $event = Event::find($id);

        return view('admin.event', compact('event'));
    }

    public function instagram()
    {
        return view('admin.instagram');
    }

    public function news()
    {
        $news = News::ordered()->get();

        $page = Page::where('name', 'news')->first();

        return view('admin.news', compact('news', 'page'));
    }

    public function newsItem($id)
    {
        $news_item = News::find($id);

        $clients = Client::active()->get()->filter(function ($client) use ($news_item) {
            return (! in_array($client->id, $news_item->clients->pluck('id')->toArray()));
        });

        $persons = Person::active()->get()->filter(function ($person) use ($news_item) {
            return (! in_array($person->id, $news_item->persons->pluck('id')->toArray()));
        });

        $tags = Tag::all()->filter(function ($person) use ($news_item) {
            return (! in_array($person->id, $news_item->tags->pluck('id')->toArray()));
        });


        return view('admin.news-item', compact('news_item', 'clients', 'persons', 'tags'));
    }

    public function tags()
    {
        $tags = Tag::ordered()->get();

        return view('admin.tags', compact('tags'));
    }

    public function tag($id)
    {
        $tag = Tag::find($id);

        return view('admin.tag', compact('tag'));
    }


}

