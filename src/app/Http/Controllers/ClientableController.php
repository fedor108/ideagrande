<?php

namespace App\Http\Controllers;

use App\Clientable;
use Illuminate\Http\Request;

class ClientableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientable = new Clientable;
        $clientable->fill($request->all());
        $clientable->save();

        return back()->with('store-clientable-success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clientable  $clientable
     * @return \Illuminate\Http\Response
     */
    public function show(Clientable $clientable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clientable  $clientable
     * @return \Illuminate\Http\Response
     */
    public function edit(Clientable $clientable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clientable  $clientable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clientable $clientable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clientable  $clientable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clientable $clientable)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach(['clientable_type', 'clientable_id', 'client_id'] as $key) {
            if (empty($request->input($key))) {
                throw new Exception("Not enough data for delete clientable: {$key}", 1);
            } else {
                $data[$key] = $request->input($key);
            }
        }

        $clientable = Clientable::where($data)->firstOrFail();

        $clientable->delete();

        return back()->with('delete-clientable-success', true);
    }
}
