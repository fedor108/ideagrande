<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Subservice;
use App\Client;
use App\Practice;
use App\Person;
use App\Page;
use App\Image;
use App\Setting;
use App\User;
use App\Project;
use App\Vacancy;
use App\Event;
use App\Instagram;
use App\News;
use App\Tag;

class PublicController extends Controller
{

    public function home($lang = null)
    {
        $page = Page::active()->where('name', 'home')->firstOrFail();

        $clients = Client::ordered()->active()->get();

        $projects = Project::ordered()->active()->get();

        $event = Event::ordered()->active()->first();

        $news = News::ordered()->active()->get();


        $instagram = new Instagram();
        $instagram_media = $instagram->getMedia();

        return view('public.home', compact('page', 'clients', 'projects', 'event', 'instagram_media', 'news', 'lang'));
    }

    public function page($name, $lang = null)
    {
        $page = Page::active()->where('name', $name)->firstOrFail();

        return view('public.page', compact('page', 'lang'));
    }

    public function about($lang = null)
    {
        $page = Page::active()->where('name', 'about')->firstOrFail();

        $persons = Person::ordered()->active()->get();

        return view('public.about', compact('page', 'persons', 'lang'));
    }

    public function services($lang = null)
    {
        $page = Page::active()->where('name', 'services')->firstOrFail();

        $services = Service::ordered()->active()->get();

        return view('public.services', compact('page', 'services', 'lang'));
    }

    public function service($name, $lang = null)
    {
        $page = Page::active()->where('name', 'services')->firstOrFail();

        $service = Service::active()->where('name', $name)->firstOrFail();

        return view('public.service', compact('service', 'page', 'lang'));
    }

    public function practices($lang = null)
    {
        $page = Page::active()->where('name', 'practices')->firstOrFail();

        $practices = Practice::ordered()->active()->get();

        return view('public.practices', compact('page', 'practices', 'lang'));
    }

    public function contacts($lang = null)
    {
        $page = Page::active()->where('name', 'contacts')->firstOrFail();

        return view('public.contacts', compact('page', 'lang'));
    }

    public function projects($lang = null)
    {
        $page = Page::active()->where('name', 'cases')->firstOrFail();

        $projects = Project::ordered()->active()->get();

        return view('public.projects', compact('page', 'projects', 'lang'));
    }

    public function project($name, $lang = null)
    {
        $page = Page::active()->where('name', 'cases')->firstOrFail();

        $project = Project::where('name', $name)->firstOrFail();

        return view('public.project', compact('project', 'page', 'lang'));
    }

    public function vacancies($lang = null)
    {
        $page = Page::active()->where('name', 'hiring')->firstOrFail();

        $vacancies = Vacancy::ordered()->active()->get();

        return view('public.vacancies', compact('page', 'vacancies', 'lang'));
    }

    public function news(Request $request, $lang = null)
    {
        $page = Page::active()->where('name', 'news')->firstOrFail();

        $news = News::ordered()
            ->year($request->input('year'))
            ->client($request->input('client'))
            ->tag($request->input('tag'))
            ->active()
            ->paginate(5);

        $years = News::getYears();

        $year = $request->input('year');

        $clients = Client::has('news')->get(['id', 'title']);

        $client = $request->input('client');

        $tags = Tag::has('news')->get();

        $tag = $request->input('tag');

        return view('public.news', compact(
            'page',
            'news',
            'years',
            'year',
            'clients',
            'client',
            'tags',
            'tag',
            'lang'
        ));
    }

    public function newsItem($name, $lang = null)
    {
        $page = Page::active()->where('name', 'news')->firstOrFail();

        $news_item = News::where('name', $name)->with('persons')->firstOrFail();

        return view('public.news-item', compact('page', 'news_item',  'lang'));
    }

}
