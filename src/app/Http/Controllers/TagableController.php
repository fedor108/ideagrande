<?php

namespace App\Http\Controllers;

use App\Tagable;
use Illuminate\Http\Request;

class TagableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tagable = new Tagable;
        $tagable->fill($request->all());
        $tagable->save();

        return back()->with('store-tagable-success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tagable  $tagable
     * @return \Illuminate\Http\Response
     */
    public function show(Tagable $tagable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tagable  $tagable
     * @return \Illuminate\Http\Response
     */
    public function edit(Tagable $tagable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tagable  $tagable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tagable $tagable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tagable  $tagable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tagable $tagable)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach(['tagable_type', 'tagable_id', 'tag_id'] as $key) {
            if (empty($request->input($key))) {
                throw new Exception("Not enough data for delete tagable: {$key}", 1);
            } else {
                $data[$key] = $request->input($key);
            }
        }

        $tagable = Tagable::where($data)->firstOrFail();

        $tagable->delete();

        return back()->with('delete-tagable-success', true);
    }
}
