<?php

namespace App\Http\Controllers;

use App\Practice;
use Illuminate\Http\Request;

class PracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $practice = new Practice;
        $practice->fill($request->all());
        $practice->save();

        return back()->with('store-practice-success', $practice->title);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function show(Practice $practice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function edit(Practice $practice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Practice $practice)
    {
        if (! empty($request->file('uploads'))) {
            $practice->addImages($request->file('uploads'));
        }

        $practice->fill($request->all());
        $practice->save();

        return back()->with('update-practice-success', $practice->title);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Practice $practice)
    {
        $practice->delete();

        return back()->with('destroy-practice-success', $practice->title);
    }

    public function sort(Request $request)
    {
        $sort = $request->input('sort');

        Practice::whereIn('id', $sort)->get()->each(function ($practice) use ($sort) {
            $practice->sort = array_search($practice->id, $sort);
            $practice->save();
        });

        return json_encode(compact('sort'));
    }
}
