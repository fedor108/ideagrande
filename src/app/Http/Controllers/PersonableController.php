<?php

namespace App\Http\Controllers;

use App\Personable;
use Illuminate\Http\Request;

class PersonableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $personable = new Personable;
        $personable->fill($request->all());
        $personable->save();

        return back()->with('store-personable-success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Personable  $personable
     * @return \Illuminate\Http\Response
     */
    public function show(Personable $personable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Personable  $personable
     * @return \Illuminate\Http\Response
     */
    public function edit(Personable $personable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Personable  $personable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Personable $personable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Personable  $personable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Personable $personable)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach(['personable_type', 'personable_id', 'person_id'] as $key) {
            if (empty($request->input($key))) {
                throw new Exception("Not enough data for delete personable: {$key}", 1);
            } else {
                $data[$key] = $request->input($key);
            }
        }

        $personable = Personable::where($data)->firstOrFail();

        $personable->delete();

        return back()->with('delete-personable-success', true);
    }
}
