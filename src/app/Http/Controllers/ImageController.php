<?php

namespace App\Http\Controllers;

use App\Image;
use Validator;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        $data = json_decode($request->input('data'), true);

        if (!empty($data['type']) && ('clippings' == $data['type'])) {
            Validator::make(
                $request->all(),
                [
                    'title' => 'required',
                    'title_ru' => 'required',
                    'description' => 'required',
                    'description_ru' => 'required',
                ],
                [
                    'title.required' => 'The publishing title En is required.',
                    'title_ru.required' => 'The publishing title Ru is required.',
                    'description.required' => 'The material title En is required.',
                    'description_ru.required' => 'The material title Ru is required.',
                ]
            )->validate();
        }

        if (! empty($request->file('upload'))) {
            $file = $request->file('upload');
            $src = $file->storeAs($image->dir, $file->getClientOriginalName());
            $image->updateSrc($src);
        }

        if (! empty($request->file('pdf'))) {
            $image->addImages($request->file('pdf'), ['type' => 'pdf']);
        }

        $image->fill($request->all());
        $image->save();

        return back()->with('update-image-success', $image->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $image->delete();

        return back()->with('destroy-image-success', $image->id);
    }

    public function sort(Request $request)
    {
        $sort = $request->input('sort');

        Image::whereIn('id', $sort)->get()->each(function ($image) use ($sort) {
            $image->sort = array_search($image->id, $sort);
            $image->save();
        });

        return json_encode(compact('sort'));
    }
}
