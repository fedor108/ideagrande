<?php
namespace App\Http\Controllers;

use App\Instagram;
use App\Setting;
use Illuminate\Http\Request;

class InstagramController extends Controller
{
    public function login()
    {
        $instagram = new Instagram;

        return redirect($instagram->getLoginUrl());
    }

    public function success(Request $request)
    {
        $instagram = new Instagram;

        if (! empty($request->input('code'))) {
            $username = $instagram->updateToken($request->input('code'));

            return redirect()->action('AdminController@instagram')
                ->with('update-instagram-success', $username);
        } else {
            $error = empty($request->input('error')) ? true : $request->input('error');

            return redirect()->action('AdminController@instagram')
                ->with('update-instagram-error', $error);
        }
    }

}
