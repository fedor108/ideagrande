<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Setting;

class MailController extends Controller
{
    public function sendReport(Request $request)
    {
        $setting = Setting::where('name', 'report-emails')->firstOrFail();
        $to = $setting->getExploded();

        if (empty($to)) {
            $result = "error";
            $message = "not found an address to send email";
            return json_encode(compact('result', 'message'));
        } else {
            $data = $request->all();

            if (empty($data)) {
                $result = "error";
                $message = "nothing for send";
                return json_encode(compact('result', 'message'));
            }

            Mail::send(['text' => 'mails.send-report'], compact('data'), function($message) use ($to) {
                $message
                    ->to($to)
                    ->subject('[ideagrande.com] Запрос с сайта')
                    ->from('mailer@neq4.ru', 'contacts');
            });

            $result = "success";
            $message = "the message has been send";

            return json_encode(compact('result', 'message'));
        }

    }

    public function sendHiring(Request $request)
    {
        $setting = Setting::where('name', 'hr-emails')->firstOrFail();
        $to = $setting->getExploded();

        if (empty($to)) {
            $result = "error";
            $message = "not found an address to send email";
            return json_encode(compact('result', 'message'));
        } else {
            $data = $request->all();

            if (empty($data)) {
                $result = "error";
                $message = "nothing for send";
                return json_encode(compact('result', 'message'));
            }

            Mail::send(['text' => 'mails.send-hiring'], compact('data'), function($message) use ($to, $request) {
                $message
                    ->to($to)
                    ->subject('[ideagrande.com] Отзыв на вакансию')
                    ->from('mailer@neq4.ru', 'contacts');
                    if (! empty($this->request->resume)) {
                        $message->attach($request->resume->getRealPath(), ['as' => $request->resume->getClientOriginalName()]);
                    }
                });

            $result = "success";
            $message = "the message has been send";

            return json_encode(compact('result', 'message'));
        }

    }

}
