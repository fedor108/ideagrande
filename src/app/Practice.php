<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Practice extends Model
{
    protected $fillable = [
        'title',
        'text',
        'data',
        'active',
        'sort',
        'title_ru',
        'text_ru'
    ];

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function addImages($files)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/practices/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(compact('src'));
        }
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function clients()
    {
        return $this->morphToMany('App\Client', 'clientable')->orderBy('sort', 'asc');
    }

    public function getDataObjAttribute()
    {
        return json_decode($this->attributes['data']);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }
}
