<?php

namespace App;

use MetzWeb\Instagram\Instagram as Api;
use App\Setting;

class Instagram
{
    public $connected;
    private $api;
    private $setting_token;
    private $setting_user_id;

    public function __construct()
    {
        $this->connected = false;

        $this->setting_token = Setting::firstOrCreate([
            'name' => 'instagram_token',
            'title' => 'Инстаграм токен'
        ]);

        $this->setting_user_id = Setting::firstOrCreate([
            'name' => 'instagram_user_id',
            'title' => 'Инстаграм аккаунт'
        ]);

        $config = [
            'apiKey' => env('INSTAGRAM_API_ID'),
            'apiSecret' => env('INSTAGRAM_API_SECRET'),
            'apiCallback' => env('INSTAGRAM_API_REDIRECT'),
        ];

        if (!empty($config['apiKey'])
            && !empty($config['apiSecret'])
            && !empty($config['apiCallback'])
        ) {
            $this->api = new Api($config);

            if (! empty($this->setting_token->value)) {
                $this->api->setAccessToken($this->setting_token->value);
                $this->connected = true;
            }
        }
    }

    public function updateToken($code)
    {
        $data = $this->api->getOAuthToken($code);

        $this->setting_token->update(['value' => $data->access_token]);
        $this->setting_user_id->update(['value' => $data->user->id]);

        return $data->user->username;
    }

    public function getMedia()
    {
        return $this->connected ? $this->api->getUserMedia() : false;
    }

    public function getLoginUrl()
    {
        return $this->api->getLoginUrl();
    }
}
