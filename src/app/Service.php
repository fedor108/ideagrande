<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'title',
        'name',
        'sort',
        'active',
        'description',
        'data',
        'title_ru',
        'description_ru',
        'seo_title',
        'seo_title_ru',
    ];

    protected static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            if (empty($model->name)) {
                $model->name = $model->id;
                $model->save();
            }
        });

        self::updated(function ($model) {
            if (empty($model->name)) {
                $model->name = $model->id;
                $model->save();
            }
        });
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function addImages($files)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/services/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(compact('src'));
        }
    }

    public function subservices()
    {
        return $this->hasMany('App\Subservice')->orderBy('sort', 'asc');
    }

    public function projects()
    {
        return $this->morphedByMany('App\Project', 'serviceable')->orderBy('sort', 'asc');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getSeoTitleAttribute()
    {
        return empty($this->attributes['seo_title']) ? $this->attributes['title'] : $this->attributes['seo_title'];
    }

    public function getSeoTitleRuAttribute()
    {
        return empty($this->attributes['seo_title_ru']) ? $this->attributes['title_ru'] : $this->attributes['seo_title_ru'];
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }

    public function getDataObjAttribute()
    {
        return json_decode($this->attributes['data']);
    }

}
