<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'title',
        'title_ru',
        'url',
        'active',
        'sort',
    ];

    public function news()
    {
        return $this->morphedByMany('App\News', 'clientable');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function addImages($files)
    {
        foreach ($files as $id => $file) {
            $path = $file->storeAs("public/clients/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(['src' => $path]);
        }
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }

}
