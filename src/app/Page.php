<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title',
        'name',
        'description',
        'text',
        'active',
        'data',
        'sort',
        'title_ru',
        'description_ru',
        'text_ru',
        'data_obj',
        'seo_title',
        'seo_title_ru',
    ];

    protected static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            if (empty($model->name)) {
                $model->name = $model->id;
                $model->save();
            }
        });

        self::updated(function ($model) {
            if (empty($model->name)) {
                $model->name = $model->id;
                $model->save();
            }
        });
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable')->orderBy('sort', 'asc');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function addImages($files)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/pages/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(compact('src'));
        }
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getDataObjAttribute()
    {
        return json_decode($this->attributes['data']);
    }

    public function setDataObjAttribute($value)
    {
        $this->attributes['data'] = json_encode($value);
    }

    public function getSlugAttribute()
    {
        return "/{$this->name}";
    }

    public function getSeoTitleAttribute()
    {
        return empty($this->attributes['seo_title']) ? $this->attributes['title'] : $this->attributes['seo_title'];
    }

    public function getSeoTitleRuAttribute()
    {
        return empty($this->attributes['seo_title_ru']) ? $this->attributes['title_ru'] : $this->attributes['seo_title_ru'];
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }

}
