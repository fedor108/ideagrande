<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    public $table = 'persons';

    protected $fillable = ['title', 'position', 'text', 'sort', 'active', 'title_ru', 'position_ru', 'text_ru', 'phone', 'email'];

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function addImages($files)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/persons/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(compact('src'));
        }
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }

    public function getDescriptionRuAttribute()
    {
        return empty($this->attributes['description_ru']) ? $this->attributes['description'] : $this->attributes['description_ru'];
    }

    public function getPositionRuAttribute()
    {
        return empty($this->attributes['position_ru']) ? $this->attributes['position'] : $this->attributes['position_ru'];
    }

}
