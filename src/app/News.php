<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    protected $fillable = [
        'name',
        'title',
        'title_ru',
        'short',
        'short_ru',
        'seo_title',
        'seo_title_ru',
        'description',
        'description_ru',
        'text',
        'text_ru',
        'contacts_title',
        'contacts_title_ru',
        'active',
        'date',
    ];

    protected static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            if (empty($model->name)) {
                $model->name = $model->id;
                $model->save();
            }
        });

        self::updated(function ($model) {
            if (empty($model->name)) {
                $model->name = $model->id;
                $model->save();
            }
        });
    }

    public function clients()
    {
        return $this->morphToMany('App\Client', 'clientable')->orderBy('sort', 'asc');
    }

    public function getClientAttribute()
    {
        return $this->clients->first();
    }

    public function persons()
    {
        return $this->morphToMany('App\Person', 'personable')->orderBy('sort', 'asc');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'tagable')->orderBy('title', 'asc');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable')->orderBy('sort', 'asc');
    }

    public function getImageAttribute()
    {
        if ($this->images->count() > 0) {
            return $this->images->first(function ($image) {
                return (isset($image->data_obj->type)
                    && ('image' == $image->data_obj->type));
                });
        } else {
            return null;
        }
    }

    public function getIconAttribute()
    {
        if ($this->images->count() > 0) {
            return $this->images->first(function ($image) {
                return (isset($image->data_obj->type)
                    && ('icon' == $image->data_obj->type));
                });
        } else {
            return null;
        }
    }

    public function addImages($files, $data = null)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/projects/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(compact('src', 'data'));
        }
    }

    public function getGalleryAttribute()
    {
        return $this->images->filter(function ($image) {
            return (isset($image->data_obj->type)
                && ('gallery' == $image->data_obj->type));
        });
    }

    public function getYearAttribute()
    {
        return $this->getYearByDate($this->date);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('date', 'desc');
    }

    public function scopeYear($query, $year = null)
    {
        if (empty($year)) {
            return $query;
        } else {
            return $query
                ->whereYear('date', '>=', $year)
                ->whereYear('date', '<', $year + 1);
        }
    }

    public function scopeClient($query, $client_id = null)
    {
        if (empty($client_id)) {
            return $query;
        } else {
            $client = Client::with('news')->find($client_id);
            return $query->whereIn('id', $client->news->pluck('id')->toArray());
        }
    }

    public function scopeTag($query, $tag_id = null)
    {
        if (empty($tag_id)) {
            return $query;
        } else {
            $tag = Tag::with('news')->find($tag_id);
            return $query->whereIn('id', $tag->news->pluck('id')->toArray());
        }
    }

    private function getYearByDate($date)
    {
        return substr($date, 0, 4);
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }

    public static function getYears()
    {
        return self::ordered()->get(['date'])->reduce(function ($carry, $item) {
            $year = substr($item->date, 0, 4);

            if (false === $carry->search($year)) {
                $carry->push($year);
            }

            return $carry;
        }, collect());
    }

}
