<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'title',
        'title_ru',
    ];

    public function news()
    {
        return $this->morphedByMany('App\News', 'tagable');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('title', 'asc');
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }
}
