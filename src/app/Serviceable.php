<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serviceable extends Model
{
    protected $fillable = ['service_id', 'serviceable_id', 'serviceable_type'];
}
