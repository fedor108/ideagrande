<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subservice extends Model
{
    protected $fillable = [
        'title',
        'text',
        'service_id',
        'sort',
        'title_ru',
        'text_ru'
    ];

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getTitleRuAttribute()
    {
        return empty($this->attributes['title_ru']) ? $this->attributes['title'] : $this->attributes['title_ru'];
    }

}
