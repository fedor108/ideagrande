<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use InterventionImage;

class Image extends Model
{
    protected $fillable = [
        'src',
        'data',
        'url',
        'title',
        'description',
        'sort',
        'title_ru',
        'description_ru',
        'data_obj',
    ];

    protected $thumbs_path = 'app/public/thumbs/';
    protected $thumbs_src = '/storage/thumbs/';

    public function imageable()
    {
        return $this->morphTo();
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable')->orderBy('sort', 'asc');
    }

    public function addImages($files, $data = null)
    {
        foreach ($files as $id => $file) {
            $src = $file->storeAs("public/images/{$this->id}", $file->getClientOriginalName());
            $this->images()->create(compact('src', 'data'));
        }
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function updateSrc($src)
    {
        Storage::delete($this->src);
        $this->src = $src;
        $this->save();
    }

    public function getDirAttribute()
    {
        return implode('/', array_slice(explode('/', $this->src), 0, -1));
    }

    public function getNameAttribute()
    {
        return implode('/', array_slice(explode('/', $this->src), -1));
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function getDataObjAttribute()
    {
        return json_decode($this->attributes['data']);
    }

    public function setDataAttribute($value)
    {
        if (is_array($value)) {
            $this->attributes['data'] = json_encode($value);
        } else {
            $this->attributes['data'] = $value;
        }
    }

    public function getAnimationAttribute()
    {
        return isset($this->data_obj->animation) ? $this->data_obj->animation : null;
    }

    public function getStyleAttribute()
    {
        return isset($this->data_obj->style) ? $this->data_obj->style : null;
    }


    public function setDataObjAttribute($value)
    {
        $this->attributes['data'] = json_encode($value);
    }

    public function thumb($width, $height = null) {
        $height = isset($height) ? $height : $width;

        $path = Storage::path($this->src);

        $thumb = InterventionImage::make($path);

        $thumb_name = "{$this->id}-{$width}x{$height}.{$this->extention}";
        $thumb_path = storage_path() . "/" . $this->thumbs_path . $thumb_name;

        if (!file_exists($thumb_path) || (filemtime($thumb_path) < filemtime($path))) {
            $thumb->fit($width, $height)->save($thumb_path);
        }

        return $this->thumbs_src . $thumb_name;
    }

    public function getExtentionAttribute()
    {
        $arr = explode('.', $this->src);
        return end($arr);
    }
}
