<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personable extends Model
{
    protected $fillable = ['person_id', 'personable_id', 'personable_type'];
}
