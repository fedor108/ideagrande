@extends('layouts.public')

@section('content')

<div class="error404 middle-container">
    <div class="error404__shape error404__shape_1 js-detail"></div>
    <div class="error404__shape error404__shape_2">
        <div class="error404__shape-inner js-detail"></div>
    </div>
    <h1 class="error404__title page-title">
        <p class="error404__title-text">Error 404</p>
        <div class="error404__title-shape">
            <div class="error404__title-shape-inner js-detail"></div>
        </div>
    </h1>
    <p class="error404__description">
        Something went wrong and
        <br>
        we can’t find this page
    </p>
</div>

@endsection