@extends('layouts.admin', ['slug' => action('PublicController@projects')])

@section('title')
    Cases
@endsection

@section('page-title')
    Cases <small>page and list</small>
@endsection

@section('breadcrumb')
    <li class="active">Cases</li>
@endsection

@section('open-site')
    <a href="{{ action('PublicController@projects') }}" target="_blank">See on site</a>
@endsection

@section('content')

@if (! empty($page))
    {!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PATCH', 'files' => true]) !!}
        <div class="box">
            @include('admin.elements.box-header', ['title' => "Page {$page->title}"])

            <div class="box-body">

                @include('admin.elements.update-page-success')

                @include('admin.elements.destroy-image-success')

                @include('admin.elements.submit-active', ['data' => $page])

                @include('admin.elements.title', ['data' => $page])

                @include('admin.elements.seo-title', ['data' => $page])

                @include('admin.elements.description', ['data' => $page])

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    {!! Form::close() !!}
@endif

<div class="box">
    <div class="box-header with-border">
        @include('admin.elements.box-header', ['title' => "Cases list"])
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if (session('destroy-project-success'))
            <div class="callout callout-success">
                <h4>Case has been deleted</h4>
                <p>{{ session('destroy-project-success') }}</p>
            </div>
        @endif

        @if (session('store-project-success'))
            <div class="callout callout-success">
                <h4>Case has been created</h4>
                <p>{{ session('store-project-success') }}</p>
            </div>
        @endif

        @if ($projects->count() > 0)
            <table id="servieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Name</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($projects as $project)
                        <tr data-id="{{ $project->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td>
                                <a href="{{ action('AdminController@project', $project) }}">{{ $project->title }}</a>
                            </td>
                            <td>
                                <span class="label label-{{ $project->active ? 'success' : 'default' }}">
                                    {{ $project->name }}
                                </span>
                            </td>
                            <td>
                                @isset($project->image)
                                    <img src="{{ Storage::url($project->image->src) }}" class="thumbnail" style="max-height: 100px">
                                @endisset
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-project-modal-{{ $project->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'ProjectController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($projects as $project)
    @include('admin.elements.destroy-project-modal')
@endforeach

@endsection

@section('pagescripts')

<script type="text/javascript">
    $('#servieces-table > tbody').sortable({
        'handle': '.handler',
        'update': function (event, ui) {
            var sort = $(this).sortable("toArray", {attribute: "data-id"});
            var token = $(this).attr('data-token');
            var url = "{{ action('ProjectController@sort') }}";
            $.post(url, {sort: sort, '_token': token}, function(result) {
                console.log(result);
            });
        }
    });
</script>

@endsection