@extends('layouts.admin')

@section('title')
    Events
@endsection

@section('page-title')
    Events <small>List</small>
@endsection

@section('breadcrumb')
    <li class="active">Events</li>
@endsection

@section('content')

<div class="box">
    @include('admin.elements.box-header', ['title' => "Events"])

    <div class="box-body">
        @if (session('destroy-event-success'))
            <div class="callout callout-success">
                <h4>Page has been deleted</h4>
                <p>{{ session('destroy-event-success') }}</p>
            </div>
        @endif

        @if (session('store-event-success'))
            <div class="callout callout-success">
                <h4>Page has been created</h4>
                <p>{{ session('store-event-success') }}</p>
            </div>
        @endif

        @if ($events->count() > 0)
            <table id="events-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($events as $event)
                        <tr data-id="{{ $event->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td class="{{ $event->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@event', $event) }}">{{ $event->title }}</a>
                            </td>
                            <td>
                                @isset($event->image)
                                    <img src="{{ Storage::url($event->image->src) }}" class="thumbnail" style="max-height: 100px">
                                @endisset
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-event-modal-{{ $event->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'EventController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($events as $event)
    @include('admin.elements.destroy-event-modal')
@endforeach

@endsection

@section('pagescripts')

<script type="text/javascript">
    $('#pages-table > tbody').sortable({
        'handle': '.handler',
        'update': function (event, ui) {
            var sort = $(this).sortable("toArray", {attribute: "data-id"});
            var token = $(this).attr('data-token');
            var url = "{{ action('EventController@sort') }}";
            $.post(url, {sort: sort, '_token': token}, function(result) {
                console.log(result);
            });
        }
    });
</script>

@endsection