@extends('layouts.admin', ['slug' => "/about"])

@section('title')
    person | {{ $person->id }}
@endsection

@section('page-title')
    Person <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/persons">People</a></li>
    <li class="active">{{ $person->title }}</li>
@endsection

@section('content')

{!! Form::model($person, ['route' => ['persons.update', $person], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $person->title])

        <div class="box-body">
            @if (session('update-person-success'))
                <div class="callout callout-success">
                    <h4>Person has been updated</h4>
                    <p>{{ session('update-person-success') }}</p>
                    <p><a href="{{ action('AdminController@persons') }}">Go to persons list</a></p>
                </div>
            @endif

            @include('admin.elements.destroy-image-success')

            @include('admin.elements.submit-active', ['data' => $person])

            @include('admin.elements.title', ['data' => $person])

            @include('admin.elements.phone-email', ['data' => $person])

            @include('admin.elements.description', ['data' => $person, 'label' => 'Должность', 'key' => 'position'])

            @include('admin.elements.text', ['data' => $person])

            @include('admin.elements.upload-image', ['data' => $person])

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
