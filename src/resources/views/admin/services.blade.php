@extends('layouts.admin', ['slug' => action('PublicController@services')])

@section('title')
    Services
@endsection

@section('page-title')
    Services <small>page and list</small>
@endsection

@section('breadcrumb')
    <li class="active">Services</li>
@endsection

@section('open-site')
    <a href="{{ action('PublicController@services') }}" target="_blank">See on site</a>
@endsection

@section('content')

@if (! empty($page))
    {!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PATCH', 'files' => true]) !!}
        <div class="box">
            @include('admin.elements.box-header', ['title' => "Page {$page->title}"])

            <div class="box-body">

                @include('admin.elements.update-page-success')

                @include('admin.elements.destroy-image-success')

                @include('admin.elements.submit-active', ['data' => $page])

                @include('admin.elements.title', ['data' => $page])

                @include('admin.elements.seo-title', ['data' => $page])

                @include('admin.elements.description', ['data' => $page])

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    {!! Form::close() !!}
@endif

<div class="box">
    @include('admin.elements.box-header', ['title' => "Services list"])

    <div class="box-body">
        @if (session('destroy-service-success'))
            <div class="callout callout-success">
                <h4>Service has been deleted</h4>
                <p>{{ session('destroy-service-success') }}</p>
            </div>
        @endif

        @if (session('store-service-success'))
            <div class="callout callout-success">
                <h4>Service has been created</h4>
                <p>{{ session('store-service-success') }}</p>
            </div>
        @endif

        @if ($services->count() > 0)
            <table id="servieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Subservices</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($services as $service)
                        <tr data-id="{{ $service->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td>
                                <a href="{{ action('AdminController@service', $service) }}">{{ $service->title }}</a>
                            </td>
                            <td>
                                <span class="label label-{{ $service->active ? 'success' : 'default' }}">
                                    {{ $service->name }}
                                </span>
                            </td>
                            <td>
                                @isset($service->image)
                                    <img class="thumbnail" style="max-height: 100px" src="{{ Storage::url($service->image->src) }}">
                                @endisset
                            </td>
                            <td>
                                @if ($service->subservices->count() > 0)
                                    {{ $service->subservices->count() }}: {{ $service->subservices->implode('title', ', ') }}
                                @endif
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-service-modal-{{ $service->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'ServiceController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($services as $service)
    @include('admin.elements.destroy-service-modal')
@endforeach

@endsection

@section('pagescripts')

<script type="text/javascript">
    $('#servieces-table > tbody').sortable({
        'handle': '.handler',
        'update': function (event, ui) {
            var sort = $(this).sortable("toArray", {attribute: "data-id"});
            var token = $(this).attr('data-token');
            var url = "{{ action('ServiceController@sort') }}";
            $.post(url, {sort: sort, '_token': token}, function(result) {
                console.log(result);
            });
        }
    });
</script>

@endsection