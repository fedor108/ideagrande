@extends('layouts.admin')

@section('title')
    setting | {{ $setting->id }}
@endsection

@section('page-title')
    Setting <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/settings">Settings</a></li>
    <li class="active">{{ $setting->title }}</li>
@endsection

@section('content')

{!! Form::model($setting, ['route' => ['settings.update', $setting], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $setting->title }}</h3>
            <div class="box-tools pull-right">
                <!-- Collapse Button -->
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if (session('update-setting-success'))
                <div class="callout callout-success">
                    <h4>Setting has been updated</h4>
                    <p>{{ session('update-setting-success') }}</p>
                    <p><a href="{{ action('AdminController@settings') }}">Go to settings list</a></p>
                </div>
            @endif

            <div class="form-group">
                @include('admin.elements.submit')
            </div>

            <div class="form-group">
                <label for="setting-title">Title</label>
                <input name="title" value="{{ $setting->title }}" type="text" class="form-control" id="setting-title">
            </div>

            <div class="form-group">
                <label for="setting-name">Name</label>
                <input name="name" value="{{ $setting->name }}" type="text" class="form-control" id="setting-name">
            </div>

            <div class="form-group">
                <label for="setting-value">Value</label>
                <textarea name="value" class="form-control" rows="10" cols="80">{{ $setting->value }}</textarea>
            </div>

            <label>File</label>
            <div class="form-group">
                @if (empty($setting->image))
                    <input type="file" name="uploads[]">
                @else
                    @include('admin.elements.image', ['image' => $setting->image])
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
