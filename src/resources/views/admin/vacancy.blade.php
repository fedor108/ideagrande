@extends('layouts.admin', ['slug' => action('PublicController@vacancies')])

@section('title')
    vacancy | {{ $vacancy->id }}
@endsection

@section('page-title')
    Vacancy <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="{{ action('AdminController@vacancies') }}">Vacancies</a></li>
    <li class="active">{{ $vacancy->title }}</li>
@endsection

@section('content')

{!! Form::model($vacancy, ['route' => ['vacancies.update', $vacancy], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $vacancy->title])

        <div class="box-body">
            @if (session('update-vacancy-success'))
                <div class="callout callout-success">
                    <h4>Обновлена Vacancy</h4>
                    <p>{{ session('update-vacancy-success') }}</p>
                    <p><a href="{{ action('AdminController@vacancies') }}">Go to vacancy list</a></p>
                </div>
            @endif

            @include('admin.elements.destroy-image-success')

            @include('admin.elements.submit-active', ['data' => $vacancy])

            @include('admin.elements.title', ['data' => $vacancy])

            @include('admin.elements.text', ['data' => $vacancy])

            @include('admin.elements.data', ['data' => $vacancy])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
