@extends('layouts.admin', ['slug' => action('PublicController@project', ['name' => $project->name])])

@section('title')
    project | {{ $project->name }}
@endsection

@section('page-title')
    Case <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/projects">Cases</a></li>
    <li class="active">{{ $project->title }}</li>
@endsection

@php
    $visual_texts = [
        'lead' => 'Lead',
        'task' => 'Task',
        'decision' => 'Decision',
        'result' => 'Result',
    ];
@endphp

@section('content')

{!! Form::model($project, ['route' => ['projects.update', $project], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $project->title])

        <!-- /.box-header -->
        <div class="box-body">

            @if (session('update-project-success'))
                <div class="callout callout-success">
                    <h4>Case has been updated</h4>
                    <p>{{ session('update-project-success') }}</p>
                    <p><a href="{{ action('AdminController@projects') }}">Go to projects list</a></p>
                </div>
            @endif

            @include('admin.elements.destroy-image-success')

            <div class="row">
                <div class="col-md-6">
                    @include('admin.elements.submit')
                </div>
                <div class="col-md-6">
                    @include('admin.elements.active', ['data' => $project])
                </div>
            </div>

            @include('admin.elements.title', ['data' => $project])

            @include('admin.elements.name-date', ['data' => $project])

            @include('admin.elements.seo-title', ['data' => $project])

            @include('admin.elements.description', ['data' => $project])

            <div class="row">
                <div class="col-md-6">
                    @include('admin.elements.input', ['data' => $project, 'key' => 'link', 'label' => 'External link'])
                </div>
                <div class="col-md-6">
                    @include('admin.elements.input', ['data' => $project, 'key' => 'video', 'label' => 'Address of video'])
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    @include('admin.elements.input', ['data' => $project, 'key' => 'people', 'label' => 'Count of event visitors'])
                </div>
                <div class="col-md-6">
                    @include('admin.elements.input', ['data' => $project, 'key' => 'coverage', 'label' => 'Coverage'])
                </div>
            </div>

            @foreach ($visual_texts as $key => $label)
                @include('admin.elements.text', ['data' => $project, 'key' => $key, 'label' => $label])
            @endforeach

            @include('admin.elements.upload-project-images', ['data' => $project])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@include('admin.elements.clients', ['data' => $project])

@include('admin.elements.services', ['data' => $project])

@endsection

@section('pagescripts')
    <script>
        $(function () {
            @foreach ($visual_texts as $key => $label)
                CKEDITOR.replace("{{ $key }}");
                CKEDITOR.replace("{{ $key }}_ru");
            @endforeach
        });
    </script>
@append
