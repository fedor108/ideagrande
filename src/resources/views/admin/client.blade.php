@extends('layouts.admin')

@section('title')
    client | {{ $client->id }}
@endsection

@section('page-title')
    Client <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/clients">Clients</a></li>
    <li class="active">{{ $client->title }}</li>
@endsection

@section('content')

{!! Form::model($client, ['route' => ['clients.update', $client], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $client->title])

        <div class="box-body">
            @if (session('update-client-success'))
                <div class="callout callout-success">
                    <h4>Client has been updated</h4>
                    <p>{{ session('update-client-success') }}</p>
                    <p><a href="{{ action('AdminController@clients') }}">Go to clients list</a></p>
                </div>
            @endif

            @include('admin.elements.destroy-image-success')

            @include('admin.elements.submit-active', ['data' => $client])

            @include('admin.elements.title', ['data' => $client])

            @include('admin.elements.input', ['data' => $client, 'key' => 'url', 'label' => 'Client site url'])

            @include('admin.elements.upload-image', ['data' => $client])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
