@extends('layouts.admin', ['slug' => action('PublicController@service', ['name' => $subservice->service->name])])

@section('title')
    subservices | {{ $subservice->id }}
@endsection

@section('page-title')
    Subservices <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/services">Services</a></li>
    <li><a href="{{ action('AdminController@service', ['id' => $subservice->service_id]) }}">{{ $subservice->service->title }}</a></li>
    <li class="active">{{ $subservice->title }}</li>
@endsection

@section('content')

{!! Form::model($subservice, ['route' => ["subservices.update", $subservice], 'method' => 'PUT']) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $subservice->title])

        <div class="box-body">
            @if (session('update-subservice-success'))
                <div class="callout callout-success">
                    <h4>Subservice has been updated</h4>
                    <p>{{ session('update-subservice-success') }}</p>
                    <p><a href="{{ action('AdminController@service', ['id' => $subservice->service->id]) }}">Перейти к услуге {{ $subservice->service->title }}</a></p>
                </div>
            @endif

            @include('admin.elements.title', ['data' => $subservice])

            @include('admin.elements.text', ['data' => $subservice, 'label' => 'Description'])

            @include('admin.elements.active', ['data' => $subservice])
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            @include('admin.elements.submit')
        </div>
        <!-- box-footer -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
