@extends('layouts.admin')

@section('title')
    users
@endsection

@section('page-title')
    Users <small>List</small>
@endsection

@section('breadcrumb')
    <li class="active">Users</li>
@endsection

@section('content')

<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        @if (session('destroy-user-success'))
            <div class="callout callout-success">
                <h4>User has been deleted</h4>
                <p>{{ session('destroy-user-success') }}</p>
            </div>
        @endif

        @if (session('store-user-success'))
            <div class="callout callout-success">
                <h4>User has been created</h4>
                <p>{{ session('store-user-success') }}</p>
            </div>
        @endif

        @if ($users->count() > 0)
            <table id="servieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td class="{{ $user->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@user', $user) }}">{{ $user->name }}</a>
                            </td>
                            <td>
                                {{ $user->email }}
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-user-modal-{{ $user->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        {!! Form::open(['action' => 'UserController@store']) !!}
            <div class="input-group">
                <div class="row">
                    <div class="col-md-3">
                        <input name="name" type="text" class="form-control" placeholder="name">
                    </div>
                    <div class="col-md-3">
                        <input name="email" type="text" class="form-control" placeholder="email">
                    </div>
                    <div class="col-md-3">
                        <input name="password" type="text" class="form-control"  placeholder="password">
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-secondary" type="submit">Create</button>
                    </div>
            </div>
        {!! Form::close() !!}
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($users as $user)
    @include('admin.elements.destroy-user-modal')
@endforeach

@endsection

@section('pagescripts')

@endsection