@extends('layouts.admin')

@section('title')
    Tags
@endsection

@section('page-title')
    Tags <small>List</small>
@endsection

@section('breadcrumb')
    <li class="active">Tags</li>
@endsection

@section('content')

<div class="box">
    @include('admin.elements.box-header', ['title' => "Tags"])

    <div class="box-body">
        @if (session('destroy-tag-success'))
            <div class="callout callout-success">
                <h4>Page has been deleted</h4>
                <p>{{ session('destroy-tag-success') }}</p>
            </div>
        @endif

        @if (session('store-tag-success'))
            <div class="callout callout-success">
                <h4>Page has been created</h4>
                <p>{{ session('store-tag-success') }}</p>
            </div>
        @endif

        @if ($tags->count() > 0)
            <table id="tags-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Title En</th>
                        <th>Title Ru</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($tags as $tag)
                        <tr data-id="{{ $tag->id }}">
                            <td>
                                <a href="{{ action('AdminController@tag', $tag) }}">{{ $tag->title }}</a>
                            </td>
                            <td>
                                {{ $tag->title }}
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-tag-modal-{{ $tag->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'TagController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($tags as $tag)
    @include('admin.elements.destroy-tag-modal')
@endforeach

@endsection
