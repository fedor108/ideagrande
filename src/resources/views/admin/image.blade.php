@php
    $title = empty($image->title) ? $image->id : $image->title;

    $is_clipping = (isset($image->data_obj->type) && ('clippings' == $image->data_obj->type));
    $is_pdf = (isset($image->data_obj->type) && ('pdf' == $image->data_obj->type));
    $is_icon = (isset($image->data_obj->type) && ('icon' == $image->data_obj->type));

    $labels = [
        'title' => 'Title',
        'description' => 'Description',
        'url' => 'Link url',
        'data' => 'Data',
        'upload' => 'Upload image',
    ];

    if ($is_clipping) {
        $labels['title'] = 'Publishing title';
        $labels['description'] = 'Material title';
    }

    if ($is_pdf) {
        $labels['upload'] = 'Upload file';
        $title = 'PDF ' . $title;
    }

@endphp

@extends('layouts.admin')

@section('title')
    image | {{ empty($image->title) ? $image->id : $image->title }}
@endsection

@section('page-title')
    Image <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="{{ action('AdminController@imageable', ['id' => $image->id]) }}">{{ $image->imageable->title }}</a></li>
    <li class="active">{{ empty($image->title) ? $image->id : $image->title }}</li>
@endsection

@section('content')


{!! Form::model($image, ['route' => ["images.update", $image], 'method' => 'PUT', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $title] )

        <div class="box-body">
            @if (session('update-image-success'))
                <div class="callout callout-success">
                    <h4>Image has been updated</h4>
                    <p>{{ session('update-image-success') }}</p>
                    <p><a href="{{ action('AdminController@imageable', ['id' => $image->id]) }}">Back to {{ $image->imageable->title }}</a></p>
                </div>
            @endif

            <div class="row">
                @if ($is_pdf)
                    @include('admin.elements.pdf-image')
                @elseif ($is_clipping)
                    @include('admin.elements.clipping-image')
                @elseif ($is_icon)
                    @include('admin.elements.icon-image')
                @else
                    @include('admin.elements.general-image')
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection

@section('modals')
    @isset($image->image)
        @include('admin.elements.destroy-image-modal', ['image' => $image->image])
    @endisset
@append
