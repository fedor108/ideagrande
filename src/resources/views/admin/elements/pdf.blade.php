<div class="attachment-block clearfix">
    @if (empty($image))
        <div class="form-group">
            <label>Загрузить pdf</label>
            <input type="file" name="pdf[]">
        </div>
    @else
        <a class="close" data-toggle="modal" href="#destroy-image-modal-{{ $image->id }}">×</a>
        <p>
            <label>Загружен pdf</label>
            <a href="{{ action('AdminController@image', ['id' => $image->id]) }}">
                {{ Storage::url($image->src) }}
            </a>
        </p>
    @endif
</div>