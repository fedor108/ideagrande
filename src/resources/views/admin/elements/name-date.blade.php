<div class="row">
    <div class="col-md-6">
        @include('admin.elements.name')
    </div>
    <div class="col-md-6">
        @include('admin.elements.date', ['label' => 'Date'])
    </div>
</div>