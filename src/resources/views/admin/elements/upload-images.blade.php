{{--
    General element to upload $data->images
    $data must be set when include this element in view
    --}}
<div class="form-group">
    <label>Upload images</label>
    {{ Form::file('uploads[]', ['multiple' => 'multiple']) }}
</div>

@if ($data->images->count() > 0)
    <div class="form-group">
        <label>Images</label>
        <div id="data-images">
            @foreach ($data->images as $image)
                @include('admin.elements.image')
            @endforeach
        </div>
    </div>
@endif

@section('modals')
    @foreach ($data->images as $image)
        @include('admin.elements.destroy-image-modal')
    @endforeach
@endsection

@section('pagescripts')
    <script type="text/javascript">
        $('#data-images').sortable({
            'update': function (event, ui) {
                var sort = $(this).sortable("toArray", {attribute: "data-id"});
                var token = "{{ csrf_token() }}";
                var url = "{{ action('ImageController@sort') }}";
                $.post(url, {sort: sort, '_token': token}, function(result) {
                    console.log(result);
                });
            }
        });
    </script>
@append