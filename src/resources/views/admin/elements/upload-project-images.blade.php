{{--
    General element to upload one $data->image
    $data must be set when include this element in view
    --}}
<div class="form-group">
    <label>Image for project list</label>
    @if (empty($data->image))
        <input type="file" name="image-uploads[]">
    @else
        @include('admin.elements.image', ['image' => $data->image])
    @endif
</div>

<div class="form-group">
    <label>Upload images to project's gallery</label>
    {{ Form::file('gallery-uploads[]', ['multiple' => 'multiple']) }}
</div>

@if ($data->gallery->count() > 0)
    <div class="form-group">
        <label>Project's gallery</label>
        <div class="data-images">
            @foreach ($data->gallery as $image)
                @include('admin.elements.image')
            @endforeach
        </div>
    </div>
@endif

<div class="form-group">
    <label>Upload images to publications gallery</label>
    {{ Form::file('clippings-uploads[]', ['multiple' => 'multiple']) }}
</div>

@if ($data->clippings->count() > 0)
    <div class="form-group">
        <label>Publications gallery</label>
        <div class="data-images">
            @foreach ($data->clippings as $image)
                @include('admin.elements.image')
            @endforeach
        </div>
    </div>
@endif

@section('modals')
    @foreach ($data->images as $image)
        @include('admin.elements.destroy-image-modal')
    @endforeach
@append

@section('pagescripts')
    <script type="text/javascript">
        $('.data-images').sortable({
            'update': function (event, ui) {
                var sort = $(this).sortable("toArray", {attribute: "data-id"});
                var token = "{{ csrf_token() }}";
                var url = "{{ action('ImageController@sort') }}";
                $.post(url, {sort: sort, '_token': token}, function(result) {
                    console.log(result);
                });
            }
        });
    </script>
@append