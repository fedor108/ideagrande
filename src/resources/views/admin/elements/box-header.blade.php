<div class="box-header with-border">
    <h3 class="box-title">{{ $title }}</h3>
    <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
        </button>
    </div>
    <!-- /.box-tools -->
</div>
<!-- /.box-header -->