{{--
    General element to edit data->name
    Data must be set when include this element in view
    --}}
<div class="form-group">
    <label for="name-{{ $data->id }}">Name</label>
    <input name="name" value="{{ $data->name }}" type="text" class="form-control" id="name-{{ $data->id }}">
</div>
