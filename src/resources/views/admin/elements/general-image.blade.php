<div class="col-md-8">
    <div class="form-group">
        @include('admin.elements.submit')
    </div>

    <div class="form-group">
        <label for="image-title">{{ $labels['title'] }} En</label>
        <input name="title" value="{{ $image->title }}" type="text" class="form-control" id="image-title">
    </div>

    <div class="form-group">
        <label for="image-title">{{ $labels['title'] }} Ru</label>
        <input name="title_ru" value="{{ $image->title_ru }}" type="text" class="form-control" id="image-title">
    </div>

    <div class="form-group">
        <label for="image-description">{{ $labels['description'] }} En</label>
        <textarea name="description" class="form-control" rows="3" cols="80">{{ $image->description }}</textarea>
    </div>

    <div class="form-group">
        <label for="image-description">{{ $labels['description'] }} Ru</label>
        <textarea name="description_ru" class="form-control" rows="3" cols="80">{{ $image->description_ru }}</textarea>
    </div>

    <div class="form-group">
        <label for="image-url">{{ $labels['url'] }}</label>
        <input name="url" value="{{ $image->url }}" type="text" class="form-control" id="image-url">
    </div>

    <div class="form-group">
        <label for="image-data">{{ $labels['data'] }}</label>
        <textarea name="data" class="form-control" rows="3" cols="80">{{ $image->data }}</textarea>
    </div>
</div>

<div class="col-md-4">
    <p>
        <a href="{{ Storage::url($image->src) }}" target="_blank">
            <img style="max-width: 100%" class="img-thumbnail" src="{{ Storage::url($image->src) }}" alt="{{ $image->name }}">
        </a>
    </p>

    <div class="form-group">
        <label>{{ $labels['upload'] }}</label>
        <input type="file" name="upload">
    </div>

    @if ($is_clipping)
        @include('admin.elements.pdf', ['image' => $image->image])
    @endif
</div>