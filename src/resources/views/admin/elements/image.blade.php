@php
    $is_clippings = (isset($image->data_obj->type) && ('clippings' == $image->data_obj->type));
@endphp
<div data-id="{{ $image->id }}" class="attachment-block clearfix">
    <a href="{{ action('AdminController@image', ['id' => $image->id]) }}">
        <img class="attachment-img" src="{{ Storage::url($image->src) }}" title="{{ $image->name }}" alt="{{ $image->id }}">
    </a>
    <div class="attachment-pushed">
        <a class="close" data-toggle="modal" href="#destroy-image-modal-{{ $image->id }}">×</a>
        @if ($is_clippings)
            <h4 class="attachment-heading">{{ $image->title ? $image->title : 'No title' }}</h4>
            <div class="attachment-text">{{ $image->description ? $image->description : 'No description' }}</div>
            <div class="attachment-text">{{ $image->url ? $image->url : 'No url' }}</div>
        @else
            <h4 class="attachment-heading">{{ $image->title ? $image->title : '' }}</h4>
            <div class="attachment-text">{{ $image->description ? $image->description : '' }}</div>
            <div class="attachment-text">{{ $image->url ? $image->url : '' }}</div>
        @endif
        <!-- /.attachment-text -->
    </div>
    <!-- /.attachment-pushed -->
</div>
