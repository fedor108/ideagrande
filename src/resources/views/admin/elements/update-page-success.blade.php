@if (session('update-page-success'))
    <div class="callout callout-success">
        <h4>Обновлена страница</h4>
        <p>{{ session('update-page-success') }}</p>
        <p><a href="{{ action('AdminController@pages') }}">Перейти к списку страниц</a></p>
    </div>
@endif
