<div class="row">
    <div class="col-md-6">
        @include('admin.elements.submit')
    </div>
    <div class="col-md-6">
        @include('admin.elements.active', ['data' => $data])
    </div>
</div>
