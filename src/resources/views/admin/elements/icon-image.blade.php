<div class="col-md-12">
    <div class="form-group">
        @include('admin.elements.submit')
    </div>

    <p>
        <a href="{{ Storage::url($image->src) }}" target="_blank">
            <img style="max-width: 100%" class="img-thumbnail" src="{{ Storage::url($image->src) }}" alt="{{ $image->name }}">
        </a>
    </p>

    <div class="form-group">
        <label>{{ $labels['upload'] }}</label>
        <input type="file" name="upload">
    </div>

    @foreach (['animate' => 'Animate', 'style' => 'Style'] as $key => $label)
        <div class="form-group">
            <label>{{ $label }}</label>
            <input
                name="data_obj[{{ $key }}]"
                value="{{ empty($image->data_obj->$key) ? '' : $image->data_obj->$key }}"
                type="text"
                class="form-control">
        </div>
    @endforeach

    <input type="hidden" name="data_obj[type]" value="icon">

</div>