{{--
    The element to set connetction $data and clietns
    $data must be set in parent view
--}}
<div class="box">
    @include('admin.elements.box-header', ['title' => "Clients"])

    <div class="box-body">

        @if (session('store-clientable-success'))
            <div class="callout callout-success">
                <h4>Link is turned on</h4>
            </div>
        @endif

        @if (session('delete-clientable-success'))
            <div class="callout callout-success">
                <h4>Link is turned off</h4>
            </div>
        @endif

        @if (($clients->count() > 0) || ($data->clients->count() > 0))
            <table id="servieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Site url</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data->clients as $client)
                        <tr>
                            <td class="{{ $client->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@client', $client) }}">{{ $client->title }}</a>
                            </td>
                            <td>
                                {{ $client->url }}
                            </td>
                            <td>
                                @if (! empty($client->image))
                                    <img src="{{ Storage::url($client->image->src) }}" style="max-height: 150px" class="thumbnail">
                                @endif
                            </td>
                            <td>
                                {{ Form::open(['action' => 'ClientableController@delete', 'method' => 'DELETE']) }}
                                    <input type="hidden" name="clientable_type" value="{{ get_class($data) }}">
                                    <input type="hidden" name="clientable_id" value="{{ $data->id }}">
                                    <input type="hidden" name="client_id" value="{{ $client->id }}">
                                    <button class="btn btn-warning" type="submit">Turn off</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach

                    @foreach ($clients as $client)
                        <tr>
                            <td class="{{ $client->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@client', $client) }}">{{ $client->title }}</a>
                            </td>
                            <td>
                                {{ $client->url }}
                            </td>
                            <td>
                                @if (! empty($client->image))
                                    <img src="{{ Storage::url($client->image->src) }}" style="max-height: 150px" class="thumbnail">
                                @endif
                            </td>
                            <td>
                                {!! Form::open(['action' => 'ClientableController@store']) !!}
                                    <input type="hidden" name="clientable_type" value="{{ get_class($data) }}">
                                    <input type="hidden" name="clientable_id" value="{{ $data->id }}">
                                    <input type="hidden" name="client_id" value="{{ $client->id }}">
                                    <button class="btn btn-secondary" type="submit">Turn on</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
