{{ Form::open(['route' => ['images.destroy', $image], 'method' => 'DELETE']) }}
    <div class="modal modal-default" id="destroy-image-modal-{{ $image->id }}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Delete image?</h4>
                </div>
                <div class="modal-body">
                    <img style="max-width: 100%" src="{{ Storage::url($image->src) }}" alt="{{ Storage::url($image->src) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-warning">Delete</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
{{ Form::close() }}