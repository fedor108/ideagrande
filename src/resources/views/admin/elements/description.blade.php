{{--
    General textareas to edit $data->$key and $data->$key_ru
    $data, $key - must be set when include this element in view
    $label = may be set
    --}}
@php
    $key = isset($key) ? $key : 'description';
    $key_ru = $key . '_ru';
    $label = isset($label) ? $label : ucfirst($key);
@endphp
<div class="row">
    <div class="form-group col-md-6">
        <label for="data-{{ $key }}">{{ $label }} En</label>
        <textarea name="{{ $key }}" class="form-control" rows="3" placeholder="value of {{ $key }}">{{ $data->$key }}</textarea>
    </div>
    <div class="form-group col-md-6">
        <label for="data-{{ $key }}-ru">{{ $label }} RU</label>
        <textarea name="{{ $key }}_ru" class="form-control" rows="3" placeholder="ru value of {{ $key }}">{{ $data->$key_ru }}</textarea>
    </div>
</div>