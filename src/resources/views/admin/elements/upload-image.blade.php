{{--
    General element to upload one $data->image
    $data must be set when include this element in view
    --}}

<div class="form-group">
    @if (empty($data->image))
        <label>Image</label>
        <input type="file" name="uploads[]">
    @else
        @include('admin.elements.image', ['image' => $data->image])
    @endif
</div>

@isset($data->images)
    @section('modals')
        @foreach ($data->images as $image)
            @include('admin.elements.destroy-image-modal')
        @endforeach
    @endsection
@endisset
