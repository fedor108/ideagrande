{{--
    General element to edit $data->active
    $data must be set when include this element in view
    --}}
<input type="hidden" name="active" value="0">
<div class="form-group">
    <div class="checkbox">
        <label>
            <input type="checkbox" name="active" value="1" {{ $data->active ? 'checked' : '' }}>
            Published
        </label>
    </div>
</div>
