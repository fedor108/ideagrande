{{--
    The element to set connetction $data and services
    $data must be set in parent view
--}}
<div class="box">
    @include('admin.elements.box-header', ['title' => "Services to link"])

    <div class="box-body">

        @if (session('store-serviceable-success'))
            <div class="callout callout-success">
                <h4>Link was turned on</h4>
            </div>
        @endif

        @if (session('delete-serviceable-success'))
            <div class="callout callout-success">
                <h4>Link was turned off</h4>
            </div>
        @endif

        @if (($services->count() > 0) || ($data->services->count() > 0))
            <table id="servieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data->services as $service)
                        <tr>
                            <td class="{{ $service->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@service', $service) }}">{{ $service->title }}</a>
                            </td>
                            <td>
                                {{ Form::open(['action' => 'ServiceableController@delete', 'method' => 'DELETE']) }}
                                    <input type="hidden" name="serviceable_type" value="{{ get_class($data) }}">
                                    <input type="hidden" name="serviceable_id" value="{{ $data->id }}">
                                    <input type="hidden" name="service_id" value="{{ $service->id }}">
                                    <button class="btn btn-warning" type="submit">Turn off</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach

                    @foreach ($services as $service)
                        <tr>
                            <td class="{{ $service->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@service', $service) }}">{{ $service->title }}</a>
                            </td>
                            <td>
                                {!! Form::open(['action' => 'ServiceableController@store']) !!}
                                    <input type="hidden" name="serviceable_type" value="{{ get_class($data) }}">
                                    <input type="hidden" name="serviceable_id" value="{{ $data->id }}">
                                    <input type="hidden" name="service_id" value="{{ $service->id }}">
                                    <button class="btn btn-secondary" type="submit">Turn on</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
