{{--
    General datepicer to edit $data->$key and $data->{$key}_ru
    $data, $key  - must be set when include this element in view
    $label - my be set
    --}}
@php
    $key = isset($key) ? $key : 'date';
    $label = isset($label) ? $label : ucfirst($key);
@endphp

 <div class="form-group">
    <label>{{ $label }}</label>
    <div class="input-group date">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input name="{{ $key }}" type="text" class="form-control pull-right" id="date-{{ $key }}-{{ $data->id }}" value="{{ substr($data->$key, 0, 10) }}">
    </div>
</div>

@section('pagescripts')
    <script>
        $(function () {
            $('#date-{{ $key }}-{{ $data->id }}').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            })
        });
    </script>
@append
