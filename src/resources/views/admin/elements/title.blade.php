{{--
    General element to edit data->title and data->title_ru
    Data must be set when include this element in view
    --}}
@php
    $key = isset($key) ? $key : 'title';
    $key_ru = $key . '_ru';
    $label = isset($label) ? $label : ucfirst($key);
@endphp
<div class="row">
    <div class="form-group col-md-6">
        <label for="{{ $key }}-{{ $data->id }}">{{ $label }} En</label>
        <input name="{{ $key }}" value="{{ $data->$key }}" type="text" class="form-control" id="{{ $key }}-{{ $data->id }}">
    </div>
    <div class="form-group col-md-6">
        <label for="{{ $key }}-ru-{{ $data->id }}">{{ $label }} Ru</label>
        <input name="{{ $key }}_ru" value="{{ $data->$key_ru }}" type="text" class="form-control" id="{{ $key }}-ru-{{ $data->id }}">
    </div>
</div>
