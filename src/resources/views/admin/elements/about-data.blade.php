{{--
    Custom element to edit $data->data of page About
    $data must be set when include this element in view
    --}}
@for ($i=0; $i < 3; $i++)
    <div class="row">
        <div class="form-group col-md-6">
            @if ($i == 0)
                <label>3 blocks En</label>
            @endif
            <textarea
                name="data_obj[list][{{ $i }}]"
                class="form-control"
                rows="3"
                placeholder=""
                >{{ empty($data->data_obj->list[$i]) ? '' : $data->data_obj->list[$i] }}</textarea>
        </div>
        <div class="form-group col-md-6">
            @if ($i == 0)
                <label>3 blocks Ru</label>
            @endif
            <textarea
                name="data_obj[list_ru][{{ $i }}]"
                class="form-control"
                rows="3"
                placeholder=""
                >{{ empty($data->data_obj->list_ru[$i]) ? '' : $data->data_obj->list_ru[$i] }}</textarea>
        </div>
    </div>
@endfor

<div class="form-group">
    <div class="checkbox">
        <label>
            <input type="checkbox" name="data_obj[list_active]" value="1" {{ empty($data->data_obj->list_active) ? '' : 'checked' }}>
            Public 3 blocks
        </label>
    </div>
</div>
