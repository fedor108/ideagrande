{{--
    The element to set connetction $data and clietns
    $data must be set in parent view
--}}
<div class="box">
    @include('admin.elements.box-header', ['title' => "Persons"])

    <div class="box-body">

        @if (session('store-personable-success'))
            <div class="callout callout-success">
                <h4>Link is turned on</h4>
            </div>
        @endif

        @if (session('delete-personable-success'))
            <div class="callout callout-success">
                <h4>Link is turned off</h4>
            </div>
        @endif

        @if (($persons->count() > 0) || ($data->persons->count() > 0))
            <table id="servieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data->persons as $person)
                        <tr>
                            <td class="{{ $person->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@person', $person) }}">{{ $person->title }}</a>
                            </td>
                            <td>{{ $person->phone }}</td>
                            <td>{{ $person->email }}</td>
                            <td>
                                @if (! empty($person->image))
                                    <img src="{{ Storage::url($person->image->src) }}" style="max-height: 150px" class="thumbnail">
                                @endif
                            </td>
                            <td>
                                {{ Form::open(['action' => 'PersonableController@delete', 'method' => 'DELETE']) }}
                                    <input type="hidden" name="personable_type" value="{{ get_class($data) }}">
                                    <input type="hidden" name="personable_id" value="{{ $data->id }}">
                                    <input type="hidden" name="person_id" value="{{ $person->id }}">
                                    <button class="btn btn-warning" type="submit">Turn off</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach

                    @foreach ($persons as $person)
                        <tr>
                            <td class="{{ $person->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@person', $person) }}">{{ $person->title }}</a>
                            </td>
                            <td>{{ $person->phone }}</td>
                            <td>{{ $person->email }}</td>
                            <td>
                                @if (! empty($person->image))
                                    <img src="{{ Storage::url($person->image->src) }}" style="max-height: 150px" class="thumbnail">
                                @endif
                            </td>
                            <td>
                                {{ Form::open(['action' => 'PersonableController@store']) }}
                                    <input type="hidden" name="personable_type" value="{{ get_class($data) }}">
                                    <input type="hidden" name="personable_id" value="{{ $data->id }}">
                                    <input type="hidden" name="person_id" value="{{ $person->id }}">
                                    <button class="btn btn-secondary" type="submit">Turn on</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
