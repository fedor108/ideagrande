{!! Form::open(compact('action')) !!}
    <div class="input-group">
        <input name="title" type="text" class="form-control" required="required">
        <span class="input-group-btn">
            <button class="btn btn-secondary" type="submit">Create</button>
        </span>
    </div>
{!! Form::close() !!}