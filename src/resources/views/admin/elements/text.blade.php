{{--
    General textarea to edit $data->$key and $data->{$key}_ru
    $data, $key  - must be set when include this element in view
    $label - my be set
    --}}
@php
    $key = isset($key) ? $key : 'text';
    $key_ru = $key . '_ru';
    $label = isset($label) ? $label : ucfirst($key);
@endphp

<div class="form-group">
    <label for="data-{{ $key }}">{{ $label }} En</label>
    <textarea name="{{ $key }}" class="form-control" rows="10" cols="80" id="data-{{ $key }}">{{ $data->$key }}</textarea>
</div>

<div class="form-group">
    <label for="data-{{ $key }}-ru">{{ $label }} Ru</label>
    <textarea name="{{ $key_ru }}" class="form-control" rows="10" cols="80" id="data-{{ $key }}-ru">{{ $data->$key_ru }}</textarea>
</div>

@section('pagescripts')
    <script>
        $(function () {
            CKEDITOR.replace("{{ $key }}");
            CKEDITOR.replace("{{ $key }}_ru");
        });
    </script>
@append
