{{--
    General element to edit data->title and data->title_ru
    Data must be set when include this element in view
    --}}
@php
    $label = isset($label) ? $label : ucfirst($key);
@endphp
<div class="form-group">
    <label for="{{ $key }}-{{ $data->id }}">{{ $label }}</label>
    <input name="{{ $key }}" value="{{ $data->$key }}" type="text" class="form-control" id="{{ $key }}-{{ $data->id }}">
</div>
