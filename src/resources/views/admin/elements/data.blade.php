{{--
    General element to edit $data->data
    $data must be set when include this element in view
    --}}
<div class="form-group">
    <label for="data-data">Data</label>
    <textarea name="data" class="form-control" rows="3" cols="80" id="data-data">{{ $data->data }}</textarea>
</div>