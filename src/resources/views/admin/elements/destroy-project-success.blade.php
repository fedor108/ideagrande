@if (session('destroy-project-success'))
    <div class="callout callout-success">
        <h4>Удален проект</h4>
        <p>{{ session('destroy-project-success') }}</p>
    </div>
@endif