{{--
    Custrom element to edit $data->data of vacancies page
    $data must be set when include this element in view
    --}}

@foreach (['lead' => 'Intro'] as $key => $label)
    <div class="row">
        <div class="form-group col-md-6">
            <label>{{ $label }} En</label>
            <textarea
                name="data_obj[{{ $key }}]"
                class="form-control"
                rows="3"
                placeholder=""
                >{{ empty($data->data_obj->$key) ? '' : $data->data_obj->$key }}</textarea>
        </div>

        @php
            $key_ru = $key . "_ru";
        @endphp

        <div class="form-group col-md-6">
            <label>{{ $label }} Ru</label>
            <textarea
                name="data_obj[{{ $key_ru }}]"
                class="form-control"
                rows="3"
                placeholder=""
                >{{ empty($data->data_obj->$key_ru) ? '' : $data->data_obj->$key_ru }}</textarea>
        </div>
    </div>
@endforeach
