@php
    $labels['upload'] = 'Upload file';
    $title = 'PDF ' . $title;
@endphp
<div class="col-md-12">
    <p>
        <a href="{{ Storage::url($image->src) }}" target="_blank">
            {{ $image->name }}
        </a>
    </p>

    <div class="form-group">
        <label>{{ $labels['upload'] }}</label>
        <input type="file" name="upload">
    </div>
</div>
