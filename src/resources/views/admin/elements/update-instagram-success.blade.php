@if (session('update-instagram-success'))
    <div class="callout callout-success">
        <h4>Подключен аккаунт Инстаграм</h4>
        <p>{{ session('update-instagram-success') }}</p>
    </div>
@endif
