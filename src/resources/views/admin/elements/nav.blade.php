@php
    $items = [
        [action('AdminController@news'), "News"],
        [action('AdminController@tags'), "Tags"],
        [action('AdminController@events'), "Events"],
        [action('AdminController@projects'), "Cases"],
        [action('AdminController@services'), "Services"],
        [action('AdminController@clients'), "Clients"],
        [action('AdminController@practices'), "Practices"],
        [action('AdminController@persons'), "People"],
        [action('AdminController@vacancies'), "Vacancies"],
        [action('AdminController@settings'), '<i class="fa fa-cogs" aria-hidden="true" title="Settings"></i>'],
        [action('AdminController@users'), '<i class="fa fa-user" aria-hidden="true" title="Users"></i>'],
    ];
@endphp
<ul class="nav navbar-nav">
    @foreach ($items as $item)
        <li class="{{ (false !== strpos(url()->current(), $item[0])) ? 'active' : '' }}">
            <a href="{{ $item[0] }}">{!! $item[1] !!}</a>
        </li>
    @endforeach
</ul>
