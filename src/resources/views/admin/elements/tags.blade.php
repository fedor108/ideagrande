{{--
    The element to set connetction $data and clietns
    $data must be set in parent view
--}}
<div class="box">
    @include('admin.elements.box-header', ['title' => "Tags"])

    <div class="box-body">

        @if (session('store-tagable-success'))
            <div class="callout callout-success">
                <h4>Tag is turned on</h4>
            </div>
        @endif

        @if (session('delete-tagable-success'))
            <div class="callout callout-success">
                <h4>Tag is turned off</h4>
            </div>
        @endif

        @if (($tags->count() > 0) || ($data->tags->count() > 0))
            <table id="tags-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Title En</th>
                        <th>Title Ru</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data->tags as $tag)
                        <tr>
                            <td>
                                <a href="{{ action('AdminController@tag', $tag) }}">{{ $tag->title }}</a>
                            </td>
                            <td>
                                {{ $tag->title_ru }}
                            </td>
                            <td>
                                {{ Form::open(['action' => 'TagableController@delete', 'method' => 'DELETE']) }}
                                    <input type="hidden" name="tagable_type" value="{{ get_class($data) }}">
                                    <input type="hidden" name="tagable_id" value="{{ $data->id }}">
                                    <input type="hidden" name="tag_id" value="{{ $tag->id }}">
                                    <button class="btn btn-warning" type="submit">Turn off</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach

                    @foreach ($tags as $tag)
                        <tr>
                            <td>
                                <a href="{{ action('AdminController@tag', $tag) }}">{{ $tag->title }}</a>
                            </td>
                            <td>
                                {{ $tag->title_ru }}
                            </td>
                            <td>
                                {!! Form::open(['action' => 'TagableController@store']) !!}
                                    <input type="hidden" name="tagable_type" value="{{ get_class($data) }}">
                                    <input type="hidden" name="tagable_id" value="{{ $data->id }}">
                                    <input type="hidden" name="tag_id" value="{{ $tag->id }}">
                                    <button class="btn btn-secondary" type="submit">Turn on</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'TagController@store'])
    </div>
</div>
<!-- /.box -->
