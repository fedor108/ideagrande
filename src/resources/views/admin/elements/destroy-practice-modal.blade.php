{{ Form::open(['route' => ['practices.destroy', $practice], 'method' => 'DELETE']) }}
    <div class="modal modal-default" id="destroy-practice-modal-{{ $practice->id }}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Delete practice?</h4>
                </div>
                <div class="modal-body">
                    <p>{{ $practice->title }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-warning">Delete</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
{{ Form::close() }}