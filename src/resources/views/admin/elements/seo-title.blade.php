{{--
    General element to edit data->seo_title and data->seo_title_ru
    Data must be set when include this element in view
    --}}
<div class="row">
    <div class="form-group col-md-6">
        <label for="seo-title-{{ $data->id }}">SEO title En</label>
        <input name="seo_title" value="{{ $data->seo_title }}" type="text" class="form-control" id="seo-title-{{ $data->id }}">
    </div>
    <div class="form-group col-md-6">
        <label for="seo-title-ru-{{ $data->id }}">SEO title Ru</label>
        <input name="seo_title_ru" value="{{ $data->seo_title_ru }}" type="text" class="form-control" id="seo-title-ru-{{ $data->id }}">
    </div>
</div>
