<div class="row">
    <div class="col-md-6">
        @include('admin.elements.input', ['key' => 'phone', 'data' => $data])
    </div>
    <div class="col-md-6">
        @include('admin.elements.input', ['key' => 'email', 'data' => $data])
    </div>
</div>
