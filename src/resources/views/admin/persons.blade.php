@extends('layouts.admin', ['slug' => "/about"])

@section('title')
    Persons
@endsection

@section('page-title')
    Persons <small>Page and list</small>
@endsection

@section('breadcrumb')
    <li class="active">Persons</li>
@endsection

@section('content')

{!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => "Page {$page->title}"])

        <div class="box-body">
            @include('admin.elements.update-page-success')

            @include('admin.elements.destroy-image-success')

            @include('admin.elements.submit-active', ['data' => $page])

            @include('admin.elements.title', ['data' => $page])

            @include('admin.elements.seo-title', ['data' => $page])

            @include('admin.elements.description', ['data' => $page])

            @include('admin.elements.text', ['data' => $page, 'label' => 'Text'])

            @include('admin.elements.about-data', ['data' => $page])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

<div class="box">
    @include('admin.elements.box-header', ['title' => "Persons list"])

    <div class="box-body">
        @if (session('destroy-person-success'))
            <div class="callout callout-success">
                <h4>Person has been deleted</h4>
                <p>{{ session('destroy-person-success') }}</p>
            </div>
        @endif

        @if (session('store-person-success'))
            <div class="callout callout-success">
                <h4>Person has been created</h4>
                <p>{{ session('store-person-success') }}</p>
            </div>
        @endif

        @if ($persons->count() > 0)
            <table id="persons-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Name / position</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($persons as $person)
                        <tr data-id="{{ $person->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td class="{{ $person->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@person', $person) }}">
                                    {{ $person->title }}<br>
                                </a>
                                {{ $person->position }}
                            </td>
                            <td>
                                @isset($person->image)
                                    <img class="thumbnail" style="max-height: 100px" src="{{ Storage::url($person->image->src) }}">
                                @endisset
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-person-modal-{{ $person->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'PersonController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($persons as $person)
    @include('admin.elements.destroy-person-modal')
@endforeach

@endsection

@section('pagescripts')

<script type="text/javascript">
    $('#persons-table > tbody').sortable({
        'handle': '.handler',
        'update': function (event, ui) {
            var sort = $(this).sortable("toArray", {attribute: "data-id"});
            var token = $(this).attr('data-token');
            var url = "{{ action('PersonController@sort') }}";
            $.post(url, {sort: sort, '_token': token}, function(result) {
                console.log(result);
            });
        }
    });
</script>

@endsection
