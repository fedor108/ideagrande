@extends('layouts.admin')

@section('title')
    instagram
@endsection

@section('page-title')
    Instagram <small>Setting</small>
@endsection

@section('breadcrumb')
    <li class="active">Instagram</li>
@endsection

@section('content')

{!! Form::open(['action' => 'InstagramController@login']) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => 'Instagram'])

        <div class="box-body">
            @include('admin.elements.update-instagram-success')

            <button class="btn btn-primary" type="submit">Connect an account</button>

        </div>
        <div class="box-footer">
            <p>{{ env('INSTAGRAM_API_ID') }}</p>
            <p>{{ env('INSTAGRAM_API_SECRET') }}</p>
            <p>{{ env('INSTAGRAM_API_REDIRECT') }}</p>
        </div>
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
