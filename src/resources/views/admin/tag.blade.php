@extends('layouts.admin')

@section('title')
    tag | {{ $tag->title }}
@endsection

@section('page-title')
    Tag <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/tags">Tags</a></li>
    <li class="active">{{ $tag->title }}</li>
@endsection

@section('content')

{!! Form::model($tag, ['route' => ['tags.update', $tag], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $tag->title])

        <div class="box-body">
            @if (session('update-tag-success'))
                <div class="callout callout-success">
                    <h4>Tag has been updated</h4>
                    <p>{{ session('update-tag-success') }}</p>
                    <p><a href="{{ action('AdminController@tags') }}">Go to tags list</a></p>
                </div>
            @endif

            @include('admin.elements.submit', ['data' => $tag])

            @include('admin.elements.title', ['data' => $tag])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
