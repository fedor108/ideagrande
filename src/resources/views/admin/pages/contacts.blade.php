@section('content')

{!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $page->title])

        <div class="box-body">
            @include('admin.elements.update-page-success')

            @include('admin.elements.destroy-image-success')

            @include('admin.elements.submit-active', ['data' => $page])

            @include('admin.elements.title', ['data' => $page])

            @include('admin.elements.seo-title', ['data' => $page])

            @include('admin.elements.description', ['data' => $page])

            @include('admin.elements.description', ['data' => $page, 'key' => 'text', 'label' => 'Contacts data'])

            @include('admin.elements.data', ['data' => $page])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
