@extends('layouts.admin')

@section('title')
    clients
@endsection

@section('page-title')
    Clients <small>List</small>
@endsection

@section('breadcrumb')
    <li class="active">Clients</li>
@endsection

@section('content')

<div class="box">
    <div class="box-body">
        @if (session('destroy-client-success'))
            <div class="callout callout-success">
                <h4>Client has been deleted</h4>
                <p>{{ session('destroy-client-success') }}</p>
            </div>
        @endif

        @if (session('store-client-success'))
            <div class="callout callout-success">
                <h4>Client has beeb created</h4>
                <p>{{ session('store-client-success') }}</p>
            </div>
        @endif

        @if ($clients->count() > 0)
            <table id="servieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Site url</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($clients as $client)
                        <tr data-id="{{ $client->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td class="{{ $client->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@client', $client) }}">{{ $client->title }}</a>
                            </td>
                            <td>
                                {{ $client->url }}
                            </td>
                            <td>
                                @if (! empty($client->image))
                                    <img src="{{ Storage::url($client->image->src) }}" style="max-height: 150px" class="thumbnail">
                                @endif
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-client-modal-{{ $client->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'ClientController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($clients as $client)
    @include('admin.elements.destroy-client-modal')
@endforeach

@endsection

@section('pagescripts')

<script type="text/javascript">
    $('#servieces-table > tbody').sortable({
        'handle': '.handler',
        'update': function (event, ui) {
            var sort = $(this).sortable("toArray", {attribute: "data-id"});
            var token = $(this).attr('data-token');
            var url = "{{ action('ClientController@sort') }}";
            $.post(url, {sort: sort, '_token': token}, function(result) {
                console.log(result);
            });
        }
    });
</script>

@endsection