@extends('layouts.admin')

@section('title')
    Pages
@endsection

@section('page-title')
    Pages <small>List</small>
@endsection

@section('breadcrumb')
    <li class="active">Pages</li>
@endsection

@section('content')

<div class="box">
    @include('admin.elements.box-header', ['title' => "Pages"])

    <div class="box-body">
        @if (session('destroy-page-success'))
            <div class="callout callout-success">
                <h4>Page has been deleted</h4>
                <p>{{ session('destroy-page-success') }}</p>
            </div>
        @endif

        @if (session('store-page-success'))
            <div class="callout callout-success">
                <h4>Page has been created</h4>
                <p>{{ session('store-page-success') }}</p>
            </div>
        @endif

        @if ($pages->count() > 0)
            <table id="pages-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Name</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($pages as $page)
                        <tr data-id="{{ $page->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td class="{{ $page->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@page', $page) }}">{{ $page->title }}</a>
                            </td>
                            <td>
                                <span class="label label-{{ $page->active ? 'success' : 'default' }}">
                                    {{ $page->name }}
                                </span>
                            </td>
                            <td>
                                {{ $page->images->count() }}
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-page-modal-{{ $page->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'PageController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($pages as $page)
    @include('admin.elements.destroy-page-modal')
@endforeach

@endsection

@section('pagescripts')

<script type="text/javascript">
    $('#pages-table > tbody').sortable({
        'handle': '.handler',
        'update': function (event, ui) {
            var sort = $(this).sortable("toArray", {attribute: "data-id"});
            var token = $(this).attr('data-token');
            var url = "{{ action('PageController@sort') }}";
            $.post(url, {sort: sort, '_token': token}, function(result) {
                console.log(result);
            });
        }
    });
</script>

@endsection