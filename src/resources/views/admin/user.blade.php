@extends('layouts.admin')

@section('title')
    user | {{ $user->id }}
@endsection

@section('page-title')
    User <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/users">Users</a></li>
    <li class="active">{{ $user->name }}</li>
@endsection

@section('content')

{!! Form::model($user, ['route' => ['users.update', $user], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $user->name])

        <div class="box-body">
            @if (session('update-user-success'))
                <div class="callout callout-success">
                    <h4>User has been updated</h4>
                    <p>{{ session('update-user-success') }}</p>
                    <p><a href="{{ action('AdminController@users') }}">Go to users list</a></p>
                </div>
            @endif

            <div class="form-group">
                @include('admin.elements.submit')
            </div>

            @include('admin.elements.input', ['data' => $user, 'key' => 'name', 'label' => 'Имя'])

            @include('admin.elements.input', ['data' => $user, 'key' => 'email', 'label' => 'Имейл'])

            @include('admin.elements.input', ['data' => $user, 'key' => 'password', 'label' => 'Пароль'])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
