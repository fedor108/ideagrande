@extends('layouts.admin', ['slug' => action('PublicController@vacancies')])

@section('title')
    Vacancies
@endsection

@section('page-title')
    Vacancies <small>Page and list</small>
@endsection

@section('breadcrumb')
    <li class="active">Vacancies</li>
@endsection

@section('content')

@if (! empty($page))
    {!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PATCH', 'files' => true]) !!}
        <div class="box">
            @include('admin.elements.box-header', ['title' => "Page {$page->title}"])

            <div class="box-body">
                @include('admin.elements.update-page-success')

                @include('admin.elements.submit-active', ['data' => $page])

                @include('admin.elements.title', ['data' => $page])

                @include('admin.elements.seo-title', ['data' => $page])

                @include('admin.elements.description', ['data' => $page])

                @include('admin.elements.vacancies-data', ['data' => $page])

                @include('admin.elements.text', ['data' => $page])

                @include('admin.elements.upload-image', ['data' => $page])

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    {!! Form::close() !!}
@endif

<div class="box">
    @include('admin.elements.box-header', ['title' => "Vacancies list"])

    <div class="box-body">
        @if (session('destroy-vacancy-success'))
            <div class="callout callout-success">
                <h4>Vacancy has been deleted</h4>
                <p>{{ session('destroy-vacancy-success') }}</p>
            </div>
        @endif

        @if (session('store-vacancy-success'))
            <div class="callout callout-success">
                <h4>Vacancy has been created</h4>
                <p>{{ session('store-vacancy-success') }}</p>
            </div>
        @endif

        @if ($vacancies->count() > 0)
            <table id="vacancies-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($vacancies as $vacancy)
                        <tr data-id="{{ $vacancy->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td class="{{ $vacancy->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@vacancy', $vacancy) }}">{{ $vacancy->title }}</a>
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-vacancy-modal-{{ $vacancy->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'VacancyController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@endsection

@section('modals')
    @foreach ($vacancies as $vacancy)
        @include('admin.elements.destroy-vacancy-modal')
    @endforeach
@endsection

@section('pagescripts')
    <script type="text/javascript">
        $('#vacancies-table > tbody').sortable({
            'handle': '.handler',
            'update': function (event, ui) {
                var sort = $(this).sortable("toArray", {attribute: "data-id"});
                var token = $(this).attr('data-token');
                var url = "{{ action('VacancyController@sort') }}";
                $.post(url, {sort: sort, '_token': token}, function(result) {
                    console.log(result);
                });
            }
        });
    </script>
@endsection