@extends('layouts.admin', ['slug' => ('home' == $page->name) ? '/' : "/{$page->name}"])

@section('title')
    page | {{ $page->name }}
@endsection

@section('page-title')
    Page <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/pages">Pages</a></li>
    <li class="active">{{ $page->title }}</li>
@endsection

@if(View::exists("admin.pages.{$page->name}"))
    @include("admin.pages.{$page->name}")
@else

    @section('content')

    {!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PATCH', 'files' => true]) !!}
        <div class="box">
            @include('admin.elements.box-header', ['title' => $page->title])

            <div class="box-body">
                @include('admin.elements.update-page-success')

                @include('admin.elements.destroy-image-success')

                @include('admin.elements.submit-active', ['data' => $page])

                @include('admin.elements.title', ['data' => $page])

                @include('admin.elements.name', ['data' => $page])

                @include('admin.elements.seo-title', ['data' => $page])

                @include('admin.elements.description', ['data' => $page])

                @include('admin.elements.text', ['data' => $page])

                @include('admin.elements.upload-images', ['data' => $page])

                @include('admin.elements.data', ['data' => $page])

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    {!! Form::close() !!}

    @endsection

@endif
