@extends('layouts.admin', ['slug' => ('home' == $event->name) ? '/' : "/{$event->name}"])

@section('title')
    event | {{ $event->title }}
@endsection

@section('page-title')
    Event <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/events">Events</a></li>
    <li class="active">{{ $event->title }}</li>
@endsection

@section('content')

{!! Form::model($event, ['route' => ['events.update', $event], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $event->title])

        <div class="box-body">
            @if (session('update-event-success'))
                <div class="callout callout-success">
                    <h4>Event has been updated</h4>
                    <p>{{ session('update-event-success') }}</p>
                    <p><a href="{{ action('AdminController@events') }}">Go to events list</a></p>
                </div>
            @endif

            @include('admin.elements.destroy-image-success')

            @include('admin.elements.submit-active', ['data' => $event])

            @include('admin.elements.title', ['data' => $event])

            @include('admin.elements.description', ['data' => $event, 'label' => 'Text', 'key' => 'text'])

            @include('admin.elements.input', ['data' => $event, 'key' => 'link', 'label' => 'External link'])

            @include('admin.elements.upload-image', ['data' => $event])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@endsection
