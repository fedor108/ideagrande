@extends('layouts.admin', ['slug' => action('PublicController@service', ['name' => $service->name])])

@section('title')
    service | {{ $service->name }}
@endsection

@section('page-title')
    Services <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/services">Services</a></li>
    <li class="active">{{ $service->title }}</li>
@endsection

@section('content')

{!! Form::model($service, ['route' => ['services.update', $service], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $service->title])

        <div class="box-body">
            @if (session('update-service-success'))
                <div class="callout callout-success">
                    <h4>Service has been updated</h4>
                    <p>{{ session('update-service-success') }}</p>
                    <p><a href="{{ action('AdminController@services') }}">Go to services list</a></p>
                </div>
            @endif

            @include('admin.elements.submit-active', ['data' => $service])

            @include('admin.elements.title', ['data' => $service])

            @include('admin.elements.name', ['data' => $service])

            @include('admin.elements.seo-title', ['data' => $service])

            @include('admin.elements.description', ['data' => $service])

            @include('admin.elements.upload-image', ['data' => $service])

            @include('admin.elements.data', ['data' => $service])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

<div class="box">
    @include('admin.elements.box-header', ['title' => "Subservices list"])

    <div class="box-body">
        @if (session('destroy-subservice-success'))
            <div class="callout callout-success">
                <h4>Subservice has been deleted</h4>
                <p>{{ session('destroy-subservice-success') }}</p>
            </div>
        @endif

        @if (session('store-subservice-success'))
            <div class="callout callout-success">
                <h4>Subservice has been created</h4>
                <p>{{ session('store-subservice-success') }}</p>
            </div>
        @endif

        @if ($service->subservices->count() > 0)
            <table id="subservieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($service->subservices as $subservice)
                        <tr data-id="{{ $subservice->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td>
                                <a href="{{ action('AdminController@subservice', ['id' => $subservice->id]) }}">{{ $subservice->title }}</a>
                            </td>
                            <td>
                                {!! $subservice->text !!}
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-subservice-modal-{{ $subservice->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            @foreach ($service->subservices as $subservice)
                @include('admin.elements.destroy-subservice-modal')
            @endforeach
        @else
            <p>No data</p>
        @endif
    </div>
    <div class="box-footer">
        {!! Form::open(['action' => 'SubserviceController@store']) !!}
            <div class="input-group">
                <input name="title" type="text" class="form-control">
                <input name="service_id" type="hidden" value="{{ $service->id }}">
                <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit">Create</button>
                </span>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('pagescripts')

<script type="text/javascript">
    $('#subservieces-table > tbody').sortable({
        'handle': '.handler',
        'update': function (event, ui) {
            var sort = $(this).sortable("toArray", {attribute: "data-id"});
            var token = $(this).attr('data-token');
            var url = "{{ action('SubserviceController@sort') }}";
            $.post(url, {sort: sort, '_token': token}, function(result) {
                console.log(result);
            });
        }
    });
</script>

@endsection