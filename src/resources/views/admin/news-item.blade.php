@extends('layouts.admin', ['slug' => action('PublicController@newsItem', ['name' => $news_item->name])])

@section('title')
    news_item | {{ $news_item->title }}
@endsection

@section('page-title')
    News item <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/news">News</a></li>
    <li class="active">{{ $news_item->title }}</li>
@endsection

@section('content')

{!! Form::model($news_item, ['route' => ['news.update', $news_item], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $news_item->title])

        <div class="box-body">
            @if (session('update-news-success'))
                <div class="callout callout-success">
                    <h4>News has been updated</h4>
                    <p>{{ session('update-news-success') }}</p>
                    <p><a href="{{ action('AdminController@news') }}">Go to news list</a></p>
                </div>
            @endif

            @include('admin.elements.destroy-image-success')

            @include('admin.elements.submit-active', ['data' => $news_item])

            @include('admin.elements.name-date', ['data' => $news_item])

            @include('admin.elements.title', ['data' => $news_item])

            @include('admin.elements.seo-title', ['data' => $news_item])

            @include('admin.elements.description', ['data' => $news_item])

            @include('admin.elements.description', ['data' => $news_item, 'label' => 'Short', 'key' => 'short'])

            @include('admin.elements.text', ['data' => $news_item, 'label' => 'Text'])

            @include('admin.elements.title', ['data' => $news_item, 'key' => 'contacts_title', 'label' => 'Contacts title'])

            @include('admin.elements.upload-news-images', ['data' => $news_item])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@include('admin.elements.clients', ['data' => $news_item])

@include('admin.elements.persons', ['data' => $news_item])

@include('admin.elements.tags', ['data' => $news_item])

@endsection
