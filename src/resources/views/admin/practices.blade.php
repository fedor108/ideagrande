@extends('layouts.admin', ['slug' => action('PublicController@practices')])

@section('title')
    Practices
@endsection

@section('page-title')
    Practices <small>Page and list</small>
@endsection

@section('breadcrumb')
    <li class="active">Practices</li>
@endsection

@section('content')

@if (! empty($page))
    {!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PATCH', 'files' => true]) !!}
        <div class="box">
            @include('admin.elements.box-header', ['title' => "Page {$page->title}"])

            <div class="box-body">

                @include('admin.elements.update-page-success')

                @include('admin.elements.submit-active', ['data' => $page])

                @include('admin.elements.title', ['data' => $page])

                @include('admin.elements.seo-title', ['data' => $page])

                @include('admin.elements.description', ['data' => $page])

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    {!! Form::close() !!}
@endif

<div class="box">
    @include('admin.elements.box-header', ['title' => "Practices list"])

    <div class="box-body">
        @if (session('destroy-practice-success'))
            <div class="callout callout-success">
                <h4>Practice has been deleted</h4>
                <p>{{ session('destroy-practice-success') }}</p>
            </div>
        @endif

        @if (session('store-practice-success'))
            <div class="callout callout-success">
                <h4>Practice has been created</h4>
                <p>{{ session('store-practice-success') }}</p>
            </div>
        @endif

        @if ($practices->count() > 0)
            <table id="practices-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($practices as $practice)
                        <tr data-id="{{ $practice->id }}">
                            <td class="handler">
                                <i class="fa fa-reorder"></i>
                            </td>
                            <td class="{{ $practice->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@practice', $practice) }}">{{ $practice->title }}</a>
                            </td>
                            <td>
                                @isset($practice->image)
                                    @include('admin.elements.image', ['image' => $practice->image])
                                @endisset
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-practice-modal-{{ $practice->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'PracticeController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@endsection

@section('modals')
    @foreach ($practices as $practice)
        @include('admin.elements.destroy-practice-modal')
    @endforeach
@endsection

@section('pagescripts')
    <script type="text/javascript">
        $('#practices-table > tbody').sortable({
            'handle': '.handler',
            'update': function (event, ui) {
                var sort = $(this).sortable("toArray", {attribute: "data-id"});
                var token = $(this).attr('data-token');
                var url = "{{ action('PracticeController@sort') }}";
                $.post(url, {sort: sort, '_token': token}, function(result) {
                    console.log(result);
                });
            }
        });
    </script>
@endsection