@extends('layouts.admin', ['slug' => action('PublicController@practices')])

@section('title')
    practice | {{ $practice->id }}
@endsection

@section('page-title')
    Практика <small>edit</small>
@endsection

@section('breadcrumb')
    <li><a href="/admin/practices">Practices</a></li>
    <li class="active">{{ $practice->title }}</li>
@endsection

@section('content')

{!! Form::model($practice, ['route' => ['practices.update', $practice], 'method' => 'PATCH', 'files' => true]) !!}
    <div class="box">
        @include('admin.elements.box-header', ['title' => $practice->title])

        <div class="box-body">
            @if (session('update-practice-success'))
                <div class="callout callout-success">
                    <h4>Practice has been updated</h4>
                    <p>{{ session('update-practice-success') }}</p>
                    <p><a href="{{ action('AdminController@practices') }}">Go to practices list</a></p>
                </div>
            @endif

            @include('admin.elements.destroy-image-success')

            @include('admin.elements.submit-active', ['data' => $practice])

            @include('admin.elements.title', ['data' => $practice])

            @include('admin.elements.text', ['data' => $practice])

            @include('admin.elements.upload-images', ['data' => $practice])
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
{!! Form::close() !!}

@include('admin.elements.clients', ['data' => $practice])


@endsection
