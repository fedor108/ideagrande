@extends('layouts.admin')

@section('title')
    settings
@endsection

@section('page-title')
    Settings <small>List</small>
@endsection

@section('breadcrumb')
    <li class="active">Settings</li>
@endsection

@section('content')

<div class="box">
    <div class="box-body">
        @if (session('destroy-setting-success'))
            <div class="callout callout-success">
                <h4>Setting has been deleted</h4>
                <p>{{ session('destroy-setting-success') }}</p>
            </div>
        @endif

        @if (session('store-setting-success'))
            <div class="callout callout-success">
                <h4>Setting has been created</h4>
                <p>{{ session('store-setting-success') }}</p>
            </div>
        @endif

        @if ($settings->count() > 0)
            <table id="servieces-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($settings as $setting)
                        <tr>
                            <td class="{{ $setting->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@setting', $setting) }}">{{ $setting->title }}</a>
                            </td>
                            <td>
                                {{ $setting->name }}
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-setting-modal-{{ $setting->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'SettingController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($settings as $setting)
    @include('admin.elements.destroy-setting-modal')
@endforeach

@endsection
