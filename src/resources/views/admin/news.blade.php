@extends('layouts.admin', ['slug' => action('PublicController@news')])

@section('title')
    News
@endsection

@section('page-title')
    News <small>List</small>
@endsection

@section('breadcrumb')
    <li class="active">News</li>
@endsection

@section('content')

@if (! empty($page))
    {!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PATCH', 'files' => true]) !!}
        <div class="box">
            @include('admin.elements.box-header', ['title' => "Page {$page->title}"])

            <div class="box-body">

                @include('admin.elements.update-page-success')

                @include('admin.elements.destroy-image-success')

                @include('admin.elements.submit-active', ['data' => $page])

                @include('admin.elements.title', ['data' => $page])

                @include('admin.elements.seo-title', ['data' => $page])

                @include('admin.elements.description', ['data' => $page])

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    {!! Form::close() !!}
@endif

<div class="box">
    @include('admin.elements.box-header', ['title' => "News list"])

    <div class="box-body">
        @if (session('destroy-news-success'))
            <div class="callout callout-success">
                <h4>News has been deleted</h4>
                <p>{{ session('destroy-news-success') }}</p>
            </div>
        @endif

        @if (session('store-news-success'))
            <div class="callout callout-success">
                <h4>News has been created</h4>
                <p>{{ session('store-news-success') }}</p>
            </div>
        @endif

        @if ($news->count() > 0)
            <table id="news-table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Client</th>
                        <th>Persons</th>
                        <th>Tags</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-token="{{ csrf_token() }}">
                    @foreach ($news as $news_item)
                        <tr data-id="{{ $news_item->id }}">
                            <td>
                                {{ substr($news_item->date, 0, 10) }}
                            </td>
                            <td class="{{ $news_item->active ? '' : 'text-muted'}}">
                                <a href="{{ action('AdminController@newsItem', $news_item) }}">{{ $news_item->title }} / {{ $news_item->title_ru }}</a>
                            </td>
                            <td>
                                {{ $news_item->clients->implode('title', ', ') }}
                            </td>
                            <td>
                                {{ $news_item->persons->implode('title', ', ') }}
                            </td>
                            <td>
                                {{ $news_item->tags->implode('title', ', ') }}
                            </td>
                            <td>
                                @isset($news_item->image)
                                    <img src="{{ Storage::url($news_item->image->src) }}" class="thumbnail" style="max-height: 100px">
                                @endisset
                            </td>
                            <td>
                                <a data-toggle="modal" href="#destroy-news-modal-{{ $news_item->id }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data</p>
        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('admin.elements.create', ['action' => 'NewsController@store'])
    </div>
    <!-- box-footer -->
</div>
<!-- /.box -->

@foreach ($news as $news_item)
    @include('admin.elements.destroy-news-modal')
@endforeach

@endsection
