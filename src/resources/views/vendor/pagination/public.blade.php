@if ($paginator->hasPages())
    <div class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="pagination__arrow pagination__arrow_prev"></a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="pagination__arrow pagination__arrow_prev"></a>
        @endif

        <div class="pagination__list">
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="pagination__item pagination__item_active">{{ $page }}</a>
                        @else
                            <a href="{{ $url }}" class="pagination__item">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </div>

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="pagination__arrow pagination__arrow_next"></a>
        @else
            <a class="pagination__arrow pagination__arrow_next"></a>
        @endif
    </div>
@endif
