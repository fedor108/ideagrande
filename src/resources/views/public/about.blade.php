@extends('layouts.public')

@section('title', ($lang ? "{$page->seo_title_ru} | " : "{$page->seo_title} | "))

@section('description', $lang ? $page->description_ru : $page->description)

@section('content')

<div class="about middle-container">
    <h2 class="about__title page-title">{{ $lang ? $page->title_ru : $page->title }}</h2>
    <div class="about__text">
        <div class="info-box">
            {!! $lang ? $page->text_ru : $page->text !!}
        </div>
    </div>
    @isset($page->data_obj->list_active)
        @if ($lang)
            @isset ($page->data_obj->list_ru)
                <div class="about__list">
                    @foreach ($page->data_obj->list_ru as $i => $item)
                        <div class="about__item">
                            <p class="about__item-number">{{ $i + 1 }}</p>
                            <p class="about__item-text">{{ $item }}</p>
                        </div>
                    @endforeach
                </div>
            @endisset
        @else
            @isset ($page->data_obj->list)
                <div class="about__list">
                    @foreach ($page->data_obj->list as $i => $item)
                        <div class="about__item">
                            <p class="about__item-number">{{ $i + 1 }}</p>
                            <p class="about__item-text">{{ $item }}</p>
                        </div>
                    @endforeach
                </div>
            @endisset
        @endif
    @endisset
</div>

@isset($persons)
    @php
        $half = ceil($persons->count() / 2);
        $chunks = $persons->chunk($half);
    @endphp
    <div class="team middle-container">
        <h2 class="team__title page-title">{{ $lang ? 'Команда' : 'Team' }}</h2>
        <div class="team__list">
            @foreach ($chunks as $chunk)
                <div class="team__column">
                    @foreach($chunk as $person)
                        <div class="team__item">
                            @isset ($person->image)
                                <img src="{{ Storage::url($person->image->src) }}" class="team__item-photo">
                            @endisset
                            <p class="team__item-name">{{ $lang ? $person->title_ru : $person->title }}</p>
                            <p class="team__item-post">{{ $lang ? $person->position_ru : $person->position }}</p>
                            <div class="team__item-desc">
                                <div class="info-box">
                                    {!! $lang ? $person->text_ru : $person->text !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
@endisset

@endsection
