@if($lang)
    <a href="{{ isset($settings['policy_ru']->image) ? Storage::url($settings['policy_ru']->image->src) : '#' }}" target="_blank" class="torquoise-link"><span class="torquoise-link__inner">Я согласен с политикой конфиденциальности</span></a>
    и даю согласие на обработку моих персональных данных.
@else
    <a href="{{ isset($settings['policy']) ? Storage::url($settings['policy']->image->src) : '#' }}" target="_blank" class="torquoise-link"><span class="torquoise-link__inner">I am familiar with the Privacy Policy</span></a>
    and agree with the processing My personal data.
@endif