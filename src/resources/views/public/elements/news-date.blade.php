{{-- Пришлось так сделать setlocale не сработало, видимо из-за контейнера --}}
@php
    if ($lang) {
        $str = str_replace(
            [
                'January',
                'February',
                'March   ',
                'April',
                'May',
                'June',
                'July',
                'September',
                'October',
                'November',
                'December',
            ],
            [
                'Январь',
                'Февраль',
                'Март',
                'Апрель',
                'Май',
                'Июнь',
                'Июль',
                'Август',
                'Сентябрь',
                'Октябрь',
                'Ноябрь',
                'Декабрь',
            ],
            $date->formatLocalized('%B %d')
        );
    } else {
        $str = $date->formatLocalized('%B %d');
    }
@endphp
{{ $str }}