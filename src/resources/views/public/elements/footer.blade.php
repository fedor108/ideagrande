<footer class="footer">
    <div class="footer__container container">
        <div class="footer__left-part">
            <p class="footer__idea-grande">&copy; PR Idea Grande 2008&ndash;{{ date('Y') }}</p>
            @if ($lang)
                {{-- @isset($settings['presentation_ru']->image)
                    <a href="{{ Storage::url($settings['presentation_ru']->image->src) }}" target="_blank" class="footer__presentation">{{ $settings['presentation_ru']->image->title }}</a>
                @endisset --}}
                @isset($settings['policy_ru']->image)
                    <a href="{{ Storage::url($settings['policy_ru']->image->src) }}" target="_blank" class="footer__privacy">{{ $settings['policy_ru']->image->title }}</a>
                @endisset
            @else
                {{-- @isset($settings['presentation']->image)
                    <a href="{{ Storage::url($settings['presentation']->image->src) }}" target="_blank" class="footer__presentation">{{ $settings['presentation']->image->title }}</a>
                @endisset --}}
                @isset($settings['policy']->image)
                    <a href="{{ Storage::url($settings['policy']->image->src) }}" target="_blank" class="footer__privacy">{{ $settings['policy']->image->title }}</a>
                @endisset
            @endif
        </div>
        @if ($lang)
            <div class="footer__development">
                <p class="footer__development-text">Сделано с</p>
                <i class="footer__development-icon"></i>
                <p class="footer__development-text footer__development-text_special">в</p>
                <a href="http://digital.neq4.ru/" targer="_blank" class="footer__development-link">нек4</a>
            </div>
        @else
            <div class="footer__development">
                <p class="footer__development-text">Made with</p>
                <i class="footer__development-icon"></i>
                <p class="footer__development-text footer__development-text_special">by</p>
                <a href="http://digital.neq4.ru/" targer="_blank" class="footer__development-link">neq4</a>
            </div>
        @endif
    </div>
</footer>
