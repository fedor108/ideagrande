<header class="header">
    <div class="header__container container">
        <div class="header__menu">
            @foreach ($pages as $page)
                @if ('home' == $page->name)
                    @continue
                @endif

                @php
                    $str = ($lang) ? $page->title_ru : $page->title;
                @endphp
                <a href="{{ $page->slug . ($lang ? '/' . $lang : '') }}"
                    class="header__menu-item {{ (false !== strpos(url()->current(), $page->slug)) ? 'header__menu-item_active' : '' }}"
                >
                    {{ str_replace(' ', "&nbsp;", strtolower($str)) }}
                </a>
            @endforeach
        </div>

        <a
            @if (url('/') != url()->current())
                href="/{{ $lang ? $lang : '' }}"
            @endif
            class="header__logo">
            <img class="header__logo-img" src="/img/logo.svg">
        </a>

        <div class="header__right-part">
            <div class="header__socials">
                @php
                    $socials_fix = [
                        'facebook' => 'fb',
                        'telegram' => 'tg',
                        'instagram' => 'ig',
                    ];
                @endphp
                @isset ($settings['social'])
                    @foreach ($settings['social']->obj as $key => $url)
                        @if (empty($url))
                            @continue
                        @endif
                        <a href="{{ $url }}" target="_blank" class="header__socials-link header__socials-link_{{ $socials_fix[$key] }}"></a>
                    @endforeach
                @endisset
            </div>
            <div class="header__languages">
                <a href="{{ $lang ? '#' : url()->current() . '/ru' }}" class="header__languages-item {{ $lang ? 'header__languages-item_active' : '' }}">rus</a>
                <a href="{{ $lang ? substr(url()->current(), 0, -3) : '#'  }}" class="header__languages-item {{ $lang ? '' : 'header__languages-item_active' }}">eng</a>
            </div>
        </div>

        <a href="#" class="header__burger js-menu-toggler">
            <span class="header__burger-line"></span>
            <span class="header__burger-line"></span>
            <span class="header__burger-line"></span>
        </a>
    </div>
</header>
