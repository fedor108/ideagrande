<div class="mobile-menu" id="mobile-menu">
    <div class="mobile-menu__list">
        <div class="mobile-menu__nav">
            @foreach ($pages as $page)
                @if ('home' == $page->name)
                    @continue
                @endif

                @php
                    $str = $lang ? $page->title_ru : $page->title;
                @endphp

                <a href="{{ $page->slug . ($lang ? '/' . $lang : '') }}"
                    class="mobile-menu__nav-item {{ (false !== strpos(url()->current(), $page->slug)) ? 'mobile-menu__nav-item_active' : '' }}">
                    {{ mb_strtolower($str) }}
                </a>
            @endforeach
        </div>
        <div class="mobile-menu__socials">
            @isset ($settings['social'])
                @foreach ($settings['social']->obj as $key => $url)
                    <a href="{{ $url }}" target="_blank" class="mobile-menu__socials-item">{{ $key }}</a>
                @endforeach
            @endisset
        </div>
    </div>
    <div class="mobile-menu__lang">
        <a href="{{ $lang ? '#' : url()->current() . '/ru' }}" class="mobile-menu__lang-item {{ $lang ? 'mobile-menu__lang-item_active' : '' }}">rus</a>
        <a href="{{ $lang ? substr(url()->current(), 0, -3) : '#'  }}" class="mobile-menu__lang-item {{ $lang ? '' : 'mobile-menu__lang-item_active' }}">eng</a>
    </div>
</div>
