@if ($lang)
    @isset($settings['policy_ru']->image)
        <div class="cookies js-cookies" style="display:none;">
            <div class="cookies__container small-container">
                <div class="cookies__text-wrap">
                    <p class="cookies__text">На этом вебсайте мы используем куки, чтобы сделать работу с сайтом удобнее. Подробнее об обработке конфиденциальных данных читайте в документе
                        <a href="{{ Storage::url($settings['policy_ru']->image->src) }}"  target="_blank" class="cookies__text-link">{{ $settings['policy_ru']->image->title }}</a>
                    </p>
                </div>
                <a href="#" class="cookies__link js-cookies-link">Я согласен</a>
            </div>
        </div>
    @endisset
@else
    @isset($settings['policy']->image)
        <div class="cookies js-cookies" style="display:none;">
            <div class="cookies__container small-container">
                <div class="cookies__text-wrap">
                    <p class="cookies__text">PR Idea Grande website uses cookies, which make the site simpler to use. Find out more about our
                        <a href="{{ Storage::url($settings['policy']->image->src) }}"  target="_blank" class="cookies__text-link">{{ $settings['policy']->image->title }}</a>
                    </p>
                </div>
                <a href="#" class="cookies__link js-cookies-link">I Agree</a>
            </div>
        </div>
    @endisset
@endif