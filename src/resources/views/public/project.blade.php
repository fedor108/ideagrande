@extends('layouts.public')

@section('title', ($lang ? "{$project->seo_title_ru} | {$page->seo_title_ru} | " : "{$project->seo_title} | {$page->seo_title} | "))

@section('description', $project->description)

@section('content')

<div class="portfolio-item">
    <div class="portfolio-item__container middle-container">
        <p class="portfolio-item__date">@include('public.elements.date', ['date' => $project->date])</p>
        <h1 class="portfolio-item__title page-title">{{ $lang ? $project->title_ru : $project->title }}</h1>
        <div class="portfolio-item__text">
            {!! $lang ? $project->lead_ru : $project->lead !!}
        </div>

        <div class="portfolio-item__info-line">
            @if ($project->clients->count() > 0)
                <div class="portfolio-item__clients">
                    <p class="portfolio-item__info-line-heading">{{ $lang ? 'Клиенты' : 'Clients' }}</p>
                    <div class="portfolio-item__clients-list">
                        @foreach($project->clients as $client)
                            <img src="/img/1x1.png" data-src="{{ isset($client->image) ? Storage::url($client->image->src) : '' }}" alt="{{ $client->title }}" class="portfolio-item__clients-logo lazyload">
                        @endforeach
                    </div>
                </div>
            @endif

            @if ($project->services->count() > 0)
                <div class="portfolio-item__services">
                    <p class="portfolio-item__info-line-heading">{{ $lang ? 'Услуги' : 'Services' }}</p>
                    <div class="portfolio-item__services-list">
                        @foreach ($project->services as $service)
                            <div class="portfolio-item__services-item">
                                <a href="{{ action('PublicController@service', ['name' => $service->name, 'lang' => $lang]) }}" class="torquoise-link">
                                    <span class="torquoise-link__inner">
                                        {{ $service->title }}
                                    </span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif

            @if (! empty($project->link))
                <div class="portfolio-item__external">
                    <p class="portfolio-item__info-line-heading">{{ $lang ? 'Внешняя ссылка' : 'External page' }}</p>
                    <div class="portfolio-item__external-list">
                        <div class="portfolio-item__external-item">
                            <a href="{{ $project->link_obj->url }}" target="_blank" class="torquoise-link">
                                <span class="torquoise-link__inner">
                                    {{ $project->link_obj->text }}
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        </div>


        @if ($project->gallery->count() > 0)
            <div class="portfolio-item__fotorama-wrap">
                <div class="fotorama" data-nav="thumbs" data-thumbwidth="60" data-thumbheight="40" data-thumbmargin="15">
                    @foreach ($project->gallery->values() as $i => $image)
                        @php
                            $description = ($lang) ? $image->description_ru : $image->description;
                        @endphp
                        <img src="{{ $image->thumb(960, 640) }}"
                            alt="" class="special-gallery__main-img"
                            data-caption="{{ ($description) ? "&copy; $description" : '' }}"
                        >
                    @endforeach
                </div>
            </div>
        @endif

        @if (! empty($project->task))
            <div class="portfolio-item__text-block">
                <h3 class="portfolio-item__text-block-heading">{{ $lang ? 'Задачи' : 'Tasks' }}</h3>
                <div class="info-box info-box_standard">
                    {!! $lang ? $project->task_ru : $project->task !!}
                </div>
            </div>
        @endif

        @if (! empty($project->decision))
            <div class="portfolio-item__text-block">
                <h3 class="portfolio-item__text-block-heading">{{ $lang ? 'Решение' : 'Idea' }}</h3>
                <div class="info-box info-box_standard">
                    {!! $lang ? $project->decision_ru : $project->decision !!}
                </div>
            </div>
        @endif

        @if (! empty($project->video))
            <div class="portfolio-item__video special-video">
                <a data-fancybox href="{{ $project->video }}" class="special-video__link">
                    <img src="http://placehold.it/960x540" alt="" class="special-video__img">
                </a>
            </div>
        @endif

        @if (! empty($project->result))
            <div class="portfolio-item__text-block">
                <h3 class="portfolio-item__text-block-heading">{{ $lang ? 'Результат' : 'Result' }}</h3>
                <div class="info-box info-box_standard">
                    {!! $lang ? $project->result_ru : $project->result !!}
                </div>
            </div>
        @endif

        <div class="portfolio-item__facts">
            @if (! empty($project->people))
                <div class="portfolio-item__facts-item">
                    <div class="portfolio-item__facts-item-count">
                        <span class="portfolio-item__facts-item-count-text">{{ $project->people }}</span>
                        <i class="portfolio-item__facts-item-count-shape js-detail"></i>
                    </div>
                    <p class="portfolio-item__facts-item-text">{{ $lang ? 'человек приняло участие в мероприятии' : 'people took part in the competition' }}</p>
                </div>
            @endif

            @if (! empty($project->coverage))
                <div class="portfolio-item__facts-item">
                    <div class="portfolio-item__facts-item-count">
                        <span class="portfolio-item__facts-item-count-text">{{ $project->coverage }}</span>
                        <i class="portfolio-item__facts-item-count-shape js-detail"></i>
                    </div>
                    <p class="portfolio-item__facts-item-text">{{ $lang ? 'общий охват мероприятия' : 'total coverage of the events' }}</p>
                </div>
            @endif
        </div>

        @php
            $clipping_count = $project->clippings->filter(function ($item) {
                return !empty($item->title) && !empty($item->description);
            })->count();
        @endphp

        @if ($clipping_count > 0)
            <div class="portfolio-item__best-clippings magazines">
                <h3 class="magazines__title">{{ $lang ? 'Лучшие публикации' : 'Best clippings' }}</h3>
                <div class="magazines__list">
                    @foreach ($project->clippings as $image)
                        @if (empty($image->title) || empty($image->description))
                            @continue;
                        @endif

                        @php
                            $url = empty($image->image) ? $image->url : Storage::url($image->image->src);
                        @endphp

                        <a href="{{ $url }}" target="_blank" class="magazines__item">
                            <div class="magazines__item-image">
                                <img src="/img/1x1.png" data-src="{{ Storage::url($image->src) }}" alt="{{ $image->title }}" class="magazines__item-img lazyload">
                            </div>
                            <p class="magazines__item-title">
                                {{ $lang ? nl2br($image->description_ru) : nl2br($image->description) }}
                                <span class="magazines__item-mag-title">{{ $lang ? $image->title_ru : $image->title }}</span>
                            </p>
                        </a>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="back-module portfolio-item__back">
            <a href="{{ action('PublicController@projects', ['lang' => $lang]) }}" class="back-module__link">{{ $lang ? 'НАЗАД В ПРОЕКТЫ' : 'BACK TO CASES' }}</a>
        </div>

    </div>
</div>

@endsection
