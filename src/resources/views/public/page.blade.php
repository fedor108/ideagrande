@extends('layouts.public')

@section('title', ($lang ? "{$page->seo_title_ru} | " : "{$page->seo_title} | "))

@section('description', $page->description)

@section('content')

{{ $page->title }}

@endsection