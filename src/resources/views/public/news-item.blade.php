@extends('layouts.public')

@section('title', ($lang ? "{$news_item->seo_title_ru} | {$page->seo_title_ru} | " : "{$news_item->seo_title} | {$page->seo_title} | "))

@section('description', $news_item->description)

@section('content')

<div class="news-inner middle-container">
    <div class="news-inner__container">

        <div class="news-inner__top">
            <p class="news-inner__date">@include('public.elements.news-date', ['date' => $news_item->date])</p>
            <h1 class="news-inner__title">
                <span class="news-inner__title-part">{{ $lang ? $news_item->title_ru : $news_item->title }}</span>
                @isset($news_item->client)
                    <span class="news-inner__title-part">{{ $lang ? $news_item->client->title_ru : $news_item->client->title }}</span>
                @endisset
            </h1>
            <div class="news-inner__desc">
                {!! nl2br($news_item->short) !!}
            </div>
        </div>

        <div class="news-inner__text-wrap">
            <div class="info-box info-box_standard">
                {!! $lang ? $news_item->text_ru : $news_item->text !!}
            </div>
        </div>

{{--
        <div class="news-inner__text-wrap">
            <h2 class="news-inner__text-title">1st — Dinner according to own scenario</h2>
            <div class="info-box info-box_standard">
                <p>Dinner according to your own scenario is a unique experience that the guests of the hotel are offered to try. You will be asked to make a delicious menu of your favorite dishes, which will be prepared for you by a personal chef and served by your personal waiter. Choose for dinner any place in one of the hotels: a quiet bay, a beach at sunset, or maybe you want to get lost among the jungle or dine on the terraces of the Anantara Layan Hotel? Maybe this evening will be the same when she says: "I agree!". Celebrate the event with a special chic - with champagne, flowers and, perhaps, a diamond?</p>
            </div>
        </div>
--}}

{{--
        <div class="news-inner__text-wrap">
            <h2 class="news-inner__text-title">2nd — A trip along the Bridge of Love</h2>
            <div class="info-box info-box_standard">
                <p>Dinner according to your own scenario is a unique experience that the guests of the hotel are offered to try. You will be asked to make a delicious menu of your favorite dishes, which will be prepared for you by a personal chef and served by your personal waiter. Choose for dinner any place in one of the hotels: a quiet bay, a beach at sunset, or maybe you want to get lost among the jungle or dine on the terraces of the Anantara Layan Hotel? Maybe this evening will be the same when she says: "I agree!". Celebrate the event with a special chic - with champagne, flowers and, perhaps, a diamond?</p>
            </div>
        </div>
 --}}
        @if ($news_item->gallery->count() > 0)
            <div class="portfolio-item__fotorama-wrap">
                <div class="fotorama" data-nav="thumbs" data-thumbwidth="60" data-thumbheight="40" data-thumbmargin="15">
                    @foreach ($news_item->gallery as $image)
                        <img src="{{ Storage::url($image->src) }}"
                            alt=""
                            class="special-gallery__main-img"
                            data-caption="{{ empty($image->description) ? '' : '© ' . $image->description }}">
                    @endforeach
                </div>
            </div>
        @endif
{{--
        <div class="news-inner__text-wrap">
            <h2 class="news-inner__text-title">3rd — A walk through the wild places in the Elephant Shelter</h2>
            <div class="info-box info-box_standard">
                <p>Dinner according to your own scenario is a unique experience that the guests of the hotel are offered to try. You will be asked to make a delicious menu of your favorite dishes, which will be prepared for you by a personal chef and served by your personal waiter. Choose for dinner any place in one of the hotels: a quiet bay, a beach at sunset, or maybe you want to get lost among the jungle or dine on the terraces of the Anantara Layan Hotel? Maybe this evening will be the same when she says: "I agree!". Celebrate the event with a special chic - with champagne, flowers and, perhaps, a diamond?</p>
            </div>
        </div>
 --}}

        @if ($news_item->persons->count() > 0)
            <div class="contacts-block">
                <p class="contacts-block__title">{{ $lang ? $news_item->contacts_title_ru : $news_item->contacts_title }}</p>
                <div class="contacts-block__list">


                    @foreach ($news_item->persons as $person)
                        <div class="contacts-block__item">
                            <div
                                class="contacts-block__item-photo"
                                style="background-image: url({{ Storage::url($person->image->src) }})"
                            ></div>
                            <div class="contacts-block__item-text-wrap">
                                <p class="contacts-block__item-name">{{ $lang ? $person->title_ru : $person->title }}</p>
                                @if (!empty($person->email))
                                    <div class="contacts-block__item-mail-wrap">
                                        <a href="mailto:{{ $person->email }}" class="contacts-block__item-mail">{{ $person->email }}</a>
                                    </div>
                                @endif
                                @if (!empty($person->phone))
                                    <p class="contacts-block__phone">{{ $person->phone }}</p>
                                @endif
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        @endif

        <div class="back-module portfolio-item__back">
            @if ($lang)
                <a href="/news/ru" class="back-module__link">НАЗАД В НОВОСТИ</a>
            @else
                <a href="/news" class="back-module__link">BACK TO NEWS</a>
            @endif
        </div>

    </div>
</div>

@endsection
