@extends('layouts.public')

@section('title', ($lang ? "{$page->seo_title_ru} | " : "{$page->seo_title} | "))

@section('description', $page->description)

@section('content')

    <div class="news container">
        <div class="news__wrap middle-container">
            <h1 class="news__title page-title">{{ $lang ? $page->title_ru : $page->title }}</h1>
            <div class="news__container">

                <div class="news__main">

                    <div class="news__list">

                        @foreach ($news as $news_item)
                            <div class="news__item">
                                <p class="news__item-date">@include('public.elements.news-date', ['date' => $news_item->date])</p>
                                <div class="news__item-title-wrap">
                                    <a href="{{ action('PublicController@newsItem', ['name' => $news_item->name, 'lang' => $lang]) }}" class="news__item-title">
                                        <span class="news__item-title-part">{{ $lang ? $news_item->title_ru : $news_item->title }}</span>
                                        @isset($news_item->client->title)
                                            <span class="news__item-title-part">{{ $lang ? $news_item->client->title_ru : $news_item->client->title }}</span>
                                        @endisset
                                    </a>
                                </div>
                                <div class="news__item-desc">
                                    {!! nl2br($news_item->short) !!}
                                </div>

                                @if (!empty($news_item->image))
                                    <div class="news__item-img-wrap">
                                        <img src="{{ Storage::url($news_item->image->src) }}" alt="" class="news__item-img">
                                    </div>
                                @endif

                                @if (!empty($news_item->icon))
                                    <div
                                        class="news__item-icon-wrap js-detail detail-animate-{{ empty($news_item->icon->animate) ? 'scale' : $news_item->icon->animate }}"
                                        style="{{ empty($news_item->icon->style) ? 'left: -35px; top: 0;' : $news_item->icon->style }}">
                                        <img src="{{ Storage::url($news_item->icon->src) }}" class="news__item-icon">
                                    </div>
                                @endif
                            </div>
                        @endforeach

                    </div>

                    {{ $news
                        ->appends(compact('year', 'client', 'tag'))
                        ->links('vendor.pagination.public') }}

                </div>


                <div class="news__sidebar">

                    <div class="news__sidebar-block">
                        <p class="news__sidebar-block-title">YEAR</p>
                        <div class="news__sidebar-block-list">
                            @foreach ($years as $item)
                                <a
                                    href="/news?year={{ $item }}"
                                    class="news__sidebar-block-item {{ ($item == $year) ? 'news__sidebar-block-item_active' : '' }}"
                                >{{ $item }}</a>
                            @endforeach
                        </div>
                    </div>



                    <div class="news__sidebar-block">
                        <p class="news__sidebar-block-title">CLIENT</p>
                        <div class="news__sidebar-block-list">
                            @foreach ($clients as $item)
                                <a
                                    href="/news?client={{ $item->id }}"
                                    class="news__sidebar-block-item {{ $client == $item->id ? 'news__sidebar-block-item_active' : '' }}">
                                    {{ $item->title }}
                                </a>
                            @endforeach
                        </div>
                    </div>



                    <div class="news__sidebar-block">
                        <p class="news__sidebar-block-title">TAGS</p>
                        <div class="news__sidebar-block-list">
                            @foreach ($tags as $item)
                                <a
                                    href="/news?tag={{ $item->id }}"
                                    class="news__sidebar-block-item {{ $tag == $item->id ? 'news__sidebar-block-item_active' : '' }}">
                                    {{ $item->title }}
                                </a>
                            @endforeach
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="news__detail-1 js-detail"></div>
        <div class="news__detail-2 js-detail"></div>
        <div class="news__detail-3 js-detail"></div>
    </div>
@endsection
