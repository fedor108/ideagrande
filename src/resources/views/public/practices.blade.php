@extends('layouts.public')

@section('title', ($lang ? "{$page->seo_title_ru} | " : "{$page->seo_title} | "))

@section('description', $lang ? $page->description_ru : $page->description)

@section('content')

<div class="practices">
    <div class="practices__container middle-container">
        <h1 class="practices__title page-title">{{ $lang ? $page->title_ru : $page->title }}</h1>
        @isset($practices)
            <div class="practices__list">
                @foreach ($practices as $i => $practice)
                    <div class="practices__item">
                        <div class="practices__item-text-wrap">
                            <h2 class="practices__item-title">{{ $lang ? $practice->title_ru : $practice->title }}</h2>
                            <div class="practices__item-desc">
                                <div class="info-box">
                                    {!! $lang ? $practice->text_ru : $practice->text !!}
                                </div>
                            </div>
                            <div class="practices__item-logo-list">
                                @foreach ($practice->clients as $client)
                                    <div class="practices__item-logo-item">
                                        <img src="{{ Storage::url($client->image->src) }}" alt="" class="practices__item-logo-item-img">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="practices__item-img-container">
                            <div class="practices__item-img-wrap">
                                @isset($practice->image)
                                    <img src="{{ Storage::url($practice->image->src) }}" class="practices__item-img" alt="">

                                    @isset($practice->images[1])
                                        <img
                                            src="{{ Storage::url($practice->images[1]['src']) }}"
                                            class="practices__item-img-icon practices__item-img-icon_{{ ($i++ % 2) ? 'top' : 'bottom' }} js-detail"
                                        >
                                    @endisset
                                @endisset
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endisset
    </div>
</div>

@endsection
