@extends('layouts.public')

@section('title', ($lang ? "{$page->seo_title_ru} | " : "{$page->seo_title} | "))

@section('description', $lang ? $page->description_ru : $page->description)

@section('content')

<div class="hiring container" style="background-image: url({{ isset($page->image) ? Storage::url($page->image->src) : '' }})">
    <div class="hiring__container middle-container">
        <h1 class="hiring__title page-title">{{ $lang ? $page->title_ru : $page->title }}</h1>
        <p class="hiring__subtitle">{{ $lang ? $page->data_obj->lead_ru : $page->data_obj->lead }}</p>
        <div class="hiring__text">
            <div class="info-box info-box_hiring">
                {!! $lang ? $page->text_ru : $page->text!!}
            </div>
        </div>
    </div>
</div>

<div class="vacancies middle-container">
    <h2 class="vacancies__title">{{ $lang ? 'Открытые вакансии' : 'Available vacancies' }}</h2>
    <div class="vacancies__list">
        @foreach ($vacancies as $vacancy)
            <div class="vacancies__item">
                <h3 class="vacancies__item-title">{{ $lang ? $vacancy->title_ru : $vacancy->title }}</h3>
                <div class="vacancies__item-desc">
                    <div class="info-box info-box_vac">
                        {!! $lang ? $vacancy->text_ru : $vacancy->text !!}
                    </div>
                </div>
                <div class="vacancies__item-detail js-detail detail-animate-move-from-{{ isset($vacancy->data_obj->type) ? $vacancy->data_obj->type : 'top' }}"></div>
            </div>
        @endforeach
    </div>
</div>

@if ($vacancies->count() > 0)
    <div class="contacts middle-container">
        <div class="contacts__container">

            <div class="contacts__form-col js-contacts-form-col">

                <div class="contacts__form-col-wrap" id="contacts-form-wrap">
                    <h3 class="contacts__form-col-title">
                        <div class="contacts__form-col-title-detail js-detail"></div>
                        <span class="contacts__form-col-title-text">{{ $lang ? 'Отправьте нам сообщение' : 'Send us a message' }}</span>
                    </h3>
                    <div class="contacts__form form">
                        <form action="/mails/send/hiring" method="post" id="vacancy-form" name="vacancy-form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form__input-container">
                                <div class="form__input-holder js-input-holder" data-error="enter your name">
                                    <label class="form__input-label">{{ $lang ? 'ФИО' : 'Full Name' }}</label>
                                    <input name="vname"
                                           type="text"
                                           class="form__input js-input"
                                           required
                                           placeholder="{{ $lang ? 'ваше имя' : 'enter your name' }}">
                                </div>
                                <div class="form__input-holder js-input-holder" data-error="enter your name">
                                    <label class="form__input-label">{{ $lang ? 'Вакансия' : 'Job Vacancy' }}</label>
                                    <div class="form__select-holder">
                                        <select class="form__select js-input" name="vvacancy">
                                            @foreach ($vacancies as $vacancy)
                                                <option value="{{ $vacancy->title_ru }}">{{ $lang ? $vacancy->title_ru : $vacancy->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form__input-container">
                                <div class="form__input-holder js-input-holder" data-error="enter your name">
                                    <label class="form__input-label">{{ $lang ? 'Имейл' : 'Email' }}</label>
                                    <input name="vemail"
                                           type="email"
                                           class="form__input js-input"
                                           required
                                           placeholder="example@mail.ru">
                                </div>
                                <div class="form__input-holder js-input-holder" data-error="enter your name">
                                    <label class="form__input-label">{{ $lang ? 'Телефон' : 'Phone' }}</label>
                                    <input name="vphone"
                                           type="tel"
                                           class="form__input js-input js-phone-input"
                                           required
                                           placeholder="+7">
                                </div>
                            </div>

                            <div class="form__input-holder js-input-holder" data-error="enter your name">
                                <label class="form__input-label">{{ $lang ? 'Сообщение' : 'Comment' }}</label>
                                <textarea name="vcomment"
                                          class="form__textarea js-input"
                                          required
                                          placeholder=""></textarea>
                            </div>

                            <div class="form__input-holder form__input-holder_upload js-file-upload-parent">
                                <div class="form__upload">
                                    <input name="resume" type="file" accept="application/pdf, application/msword" class="form__upload-input js-file-upload-input">
                                    <span class="form__upload-link js-file-upload-trigger">{{ $lang ? 'Загрузить резюме' : 'Upload resume' }}</span>
                                    <span class="form__upload-info">
                                    <span class="form__upload-descr form__upload-descr_tip js-file-upload-file-name" data-default="pdf or docx less than 2MB">
                                        {{ $lang ? 'pdf или docx не более 2МБ' : 'pdf оr docx less than 2MB' }}
                                    </span>
                                    <span class="form__upload-clear js-file-upload-remove"></span>
                                </span>

                                </div>
                                <span class="form__input-error js-file-upload-error" style="display: none;"></span>
                            </div>

                            <div class="form__checkbox-holder">
                                <div class="form__checkbox">
                                    <input name="vagree" type="checkbox" class="js-checkbox" id="vagree">
                                </div>
                                <div class="form__checkbox-text">
                                    @include('public.elements.policy-checkbox-text')
                                </div>
                            </div>

                            <button class="btn js-button" type="submit" id="vsubmit">
                                <div class="btn__bg"></div>
                                <span class="btn__text">{{ $lang ? 'Отправить' : 'Send' }}</span>
                            </button>

                        </form>
                    </div>
                </div>

                <div class="contacts__form-success" id="contacts-form-message">
                    <div class="contacts__form-success-detail js-detail"></div>
                    <p class="contacts__form-success-text">{{ $lang ? 'Сообщение отправлено' : 'Your message was successfully sent' }}</p>
                    <a href="#" class="contacts__form-success-link" id="contacts-form-message-link">{{ $lang ? 'Отправить еще сообщение' : 'Send another message' }}</a>
                </div>

            </div>

        </div>
    </div>
@endif

@endsection
