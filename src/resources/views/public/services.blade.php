@extends('layouts.public')

@section('title', ($lang ? "{$page->seo_title_ru} | " : "{$page->seo_title} | "))

@section('description', $lang ? $page->description_ru : $page->description)

@section('content')

<div class="services container">
    <div class="services__container middle-container">
        <h1 class="services__title page-title">{{ $lang ? $page->title_ru : $page->title }}</h1>
        <div class="services__list">
            @php
                $half = ceil($services->count() / 2);
                $chunks = $services->chunk($half);
            @endphp

            @foreach ($chunks as $chunk)
                <div class="services__list-column">
                    @foreach ($chunk->values() as $i => $service)
                        <div class="services__item">
                            <div class="services__item-title-wrap">
                                <a href="{{ action('PublicController@service', ['name' => $service->name, 'lang' => $lang]) }}" class="services__item-title">
                                    {{ $lang ? $service->title_ru : $service->title }}
                                </a>
                                @php
                                    if (0 == $i) {
                                        $transormation = 'detail-animate-scale detail-transform-origin-left-bottom';
                                    } elseif (1 == $i) {
                                        $transormation = 'detail-animate-move-from-left';
                                    } elseif (2 == $i) {
                                        $transormation = 'detail-animate-move-from-bottom';
                                    }

                                    // $style = empty($service->data->style) ? 'left: 230px; top: -18px;' : $service->data->style;
                                    $style = empty($service->data_obj->style) ? 'empty' : $service->data_obj->style;
                                @endphp
                                <div class="services__item-icon-wrap js-detail {{ $transormation }}" style="{{ $style }}">
                                    @isset ($service->image)
                                        <img src="{{ Storage::url($service->image->src) }}" class="services__item-icon">
                                    @endisset
                                </div>
                            </div>
                            @if ($service->subservices->count() > 0)
                                <ul class="services__item-list">
                                    @foreach ($service->subservices as $subservice)
                                        <li class="services__item-list-item">{{ $lang ? $subservice->title_ru : $subservice->title }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
    <div class="services__detail-1 js-detail"></div>
    <div class="services__detail-2 js-detail"></div>
    <div class="services__detail-3 js-detail"></div>
</div>


@endsection