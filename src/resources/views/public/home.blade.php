@extends('layouts.public')

@section('description', $lang ? $page->description_ru : $page->description)

@section('content')

    <div class="who-we-are container container_special">
        <div class="who-we-are__container small-container">
            <h2 class="who-we-are__title block-title">{{ $lang ? 'кто мы' : 'who we are' }}</h2>
            <div class="who-we-are__text">
                <div class="info-box">
                    {!! $lang ? $page->text_ru : $page->text !!}
                </div>
            </div>
            <div class="who-we-are__detail-1 js-detail"></div>
            <div class="who-we-are__detail-2 js-detail"></div>
        </div>
        <div class="who-we-are__detail-3 js-detail"></div>
    </div>

    @if ($projects->count() > 0)
        <div class="cases">
            <div class="cases__container container">
                <div class="cases__bg fade-block"></div>
                <div class="cases__content">
                    <h2 class="cases__title block-title">{{ $lang ? 'что мы делаем' : 'what we do' }}</h2>
                    <div class="cases__list">

                        <div class="cases__column">
                            @php
                                $project = $projects[0];
                            @endphp
                            <a href="{{ action('PublicController@project', ['name' => $project->name, 'lang' => $lang]) }}"
                                class="cases__item cases__item_bg"
                                style="background-image: url({{ isset($project->image) ? $project->image->thumb(305, 528) : '' }})">
                                <p class="cases__item-date">@include('public.elements.date', ['date' => $project->date])</p>
                                <p class="cases__item-title">
                                    <span class="cases__item-title-part">{{ $lang ? $project->title_ru : $project->title }}</span>
                                    <span class="cases__item-title-part">{{ $project->clients->implode($lang ? 'title_ru' : 'title', ', ') }}</span>
                                </p>
                            </a>
                        </div>

                        @isset($projects[1])
                            @php
                                $project = $projects[1];
                            @endphp
                            <div class="cases__column">
                                <a href="{{ action('PublicController@project', ['name' => $project->name, 'lang' => $lang]) }}" class="cases__item">
                                    <div class="cases__item-title">
                                        <p class="cases__item-title-part">{{ $lang ? $project->title_ru : $project->title }}</p>
                                        <p class="cases__item-title-part">{{ $project->clients->implode($lang ? 'title_ru' : 'title', ', ') }}</p>
                                    </div>
                                    <div class="cases__item-image"
                                        style="background-image: url({{ isset($project->image) ? $project->image->thumb(240) : '' }})">
                                        <p class="cases__item-date">@include('public.elements.date', ['date' => $project->date])</p>
                                        <div class="cases__item-detail js-detail"></div>
                                    </div>
                                </a>
                                <div class="cases__fact">
                                    <p class="cases__fact-text">{{ $lang ?  $page->data_obj->releases_ru : $page->data_obj->releases }}</p>
                                    <div class="cases__fact-detail js-detail"></div>
                                </div>
                            </div>
                        @endisset

                        @isset($projects[2])
                            @php
                                $project = $projects[2];
                            @endphp
                            <div class="cases__column">
                                <div class="cases__fact">
                                    <p class="cases__fact-text">{{ $lang ?  $page->data_obj->events_ru : $page->data_obj->events }}</p>
                                    <div class="cases__fact-detail js-detail"></div>
                                </div>
                                <a href="{{ action('PublicController@project', ['name' => $project->name, 'lang' => $lang]) }}" class="cases__item">
                                    <div class="cases__item-image"
                                        style="background-image: url({{ isset($project->image) ? $project->image->thumb(480, 340) : '' }})">
                                        <p class="cases__item-date">@include('public.elements.date', ['date' => $project->date])</p>
                                        <div class="cases__item-detail js-detail"></div>
                                    </div>
                                    <div class="cases__item-title">
                                        <p class="cases__item-title-part">{{ $lang ? $project->title_ru : $project->title }}</p>
                                        <p class="cases__item-title-part">{{ $project->clients->implode($lang ? 'title_ru' : 'title', ', ') }}</p>
                                    </div>
                                </a>
                            </div>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    @endif

    @isset($news)
        <div class="news-block">
            <div class="news-block__container container">
                <div class="news-block__content">

                    <div class="news-block__top-line small-container">
                        <h2 class="news-block__title block-title">{{ $lang ? 'НОВОСТИ' : 'NEWS' }}</h2>
                        <div class="news-block__slider-nav">
                            <a href="#" class="news-block__slider-arrow news-block__slider-arrow_prev"></a>
                            <a href="#" class="news-block__slider-arrow news-block__slider-arrow_next"></a>
                        </div>
                    </div>

                    <div class="news-block__slider-wrap ">
                        <div class="news-block__slider swiper-container" id="news-block-slider">
                            <div class="swiper-wrapper">

                                @foreach ($news->chunk(2) as $news_chunk)
                                    <div class="swiper-slide">
                                        <div class="news-block__list">

                                            @foreach ($news_chunk as $news_item)
                                                <div class="news-block__item">
                                                    <p class="news-block__item-date">@include('public.elements.news-date', ['date' => $news_item->date])</p>
                                                    <a href="{{ action('PublicController@newsItem', ['name' => $news_item->name, 'lang' => $lang]) }}" class="news-block__item-title">
                                                        <span class="news-block__item-title-part">
                                                            {{ $lang ? $news_item->title_ru : $news_item->title }}
                                                        </span>
                                                        @isset($news_item->client)
                                                            <span class="news-block__item-title-part">{{ $lang ? $news_item->client->title_ru : $news_item->client->title }}</span>
                                                        @endisset
                                                    </a>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>

{{--                     <div class="news-block__subscribe small-container">
                        <form>
                            <div class="news-block__form-container">
                                <div class="news-block__input-holder">
                                    <div class="form__input-holder js-input-holder" data-error="enter your email">
                                        <label class="form__input-label">Subscribe to our Newsletter</label>
                                        <input name="cemail"
                                               type="email"
                                               class="form__input js-input"
                                               required
                                               placeholder="E-mail">
                                    </div>
                                </div>
                                <button type="submit" class="news-block__subscribe-button">SUBSCRIBE</button>
                            </div>

                        </form>
                    </div>
 --}}
                </div>
            </div>
        </div>
    @endisset

    @isset ($event)
        <a
            @if (! empty($event->link))
                href="{{ $event->link }}"
            @endif
            class="feature-event">
            <div class="feature-event__container container">
                <div class="feature-event__bg" style="background-image: url({{ empty($event->image) ? '' : Storage::url($event->image->src) }})">
                    <div class="feature-event__text-wrap small-container">
                        <h2 class="feature-event__title block-title">{{ $lang ? 'будущие событие' : 'feature event' }}</h2>
                        <p class="feature-event__desc">
                            {!! $lang ? nl2br($event->text_ru) : nl2br($event->text) !!}
                        </p>
                        <p class="feature-event__info">{{ $lang ? $event->title_ru : $event->title }}</p>
                        <div class="feature-event__detail js-detail"></div>
                    </div>
                </div>
            </div>
        </a>
    @endisset

    @if (! empty($instagram_media->data))
        <div class="instagram container container_special">
            <h2 class="instagram__title block-title small-container">{{ $lang ? 'инстаграм' : 'instagram cast' }}</h2>
            <div class="instagram__wrap">
                <div class="instagram__main-photo">
                    <img class="instagram__main-photo-img" src="{{ $instagram_media->data[0]->images->low_resolution->url }}">
                    <div class="instagram__main-photo-detail js-detail"></div>
                </div>
                @if (count($instagram_media->data) > 5)
                    <div class="instagram__list">
                        @for ($i = 1; $i < 5; $i++)
                            <div class="instagram__item">
                                <img class="instagram__item-img" src="{{ $instagram_media->data[$i]->images->low_resolution->url }}">
                            </div>
                        @endfor
                    </div>
                    <div class="instagram__link-wrap">
                        <div class="instagram__link" style="background-image: url({{ $instagram_media->data[$i]->images->low_resolution->url }})">
                        </div>
                        <div class="instagram__link-info">
                            <div class="instagram__link-detail js-detail"></div>
                            <a href="{{ $settings['instagram']->value }}" target="_blank" class="instagram__link-text">{{ $lang ? 'Больше снимков на' : 'More shots on' }} @pr_ideagrande</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    @endif

    <div class="clients">
        @isset($clients)
            <div class="small-container">
                <h2 class="clients__title block-title small-container">{{ $lang ? 'клиенты' : 'clients' }}</h2>
            </div>

            <div class="clients__list middle-container">
                @foreach ($clients as $client)
                    <div class="clients__item-wrap">
                        <a
                            @if (! empty($client->url))
                                href="{{ $client->url }}"
                            @endif
                            class="clients__item">
                            <img class="clients__item-logo" src="{{ Storage::url($client->image->src) }}" alt="{{ $client->title }}">
                        </a>
                    </div>
                @endforeach
            </div>
        @endisset

        @isset($page->data_obj->brands)
            <div class="small-container">
                <div class="clients__text-wrap">
                    <div class="clients__text">
                        {{ $lang ? $page->data_obj->brands_ru : $page->data_obj->brands }}
                    </div>
                    <i class="clients__text-shape js-detail"></i>
                </div>
            </div>
        @endisset
    </div>

@endsection
