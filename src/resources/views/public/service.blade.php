@extends('layouts.public')

@section('title', ($lang ? "{$service->seo_title_ru} | {$page->seo_title_ru} | " : "{$service->seo_title} | {$page->seo_title} | "))

@section('description', $lang ? $service->description_ru : $service->description)

@section('content')

<div class="serv-inner">
    <div class="serv-inner__container middle-container">
        <h1 class="serv-inner__title page-title">{{ $lang ? $service->title_ru : $service->title }}</h1>
        <div class="serv-inner__content">
            <div class="serv-inner__desc">
                <div class="info-box">
                    @foreach ($service->subservices as $subservice)
                        <h2>{{ $lang ? $subservice->title_ru : $subservice->title }}</h2>
                        {!! $lang ? $subservice->text_ru : $subservice->text !!}
                    @endforeach
                </div>
            </div>

            @if ($service->projects->count() > 0)
                <div class="serv-inner__cases">
                    <p class="serv-inner__cases-title">{{ $lang ? 'связанные проекты' : 'related cases' }}</p>
                    <div class="serv-inner__cases-list">

                        @php
                            $project = $service->projects[0];
                        @endphp
                        <div class="cases__column">
                            <a href="{{ action('PublicController@project', ['name' => $project->name, 'lang' => $lang]) }}"
                                class="cases__item cases__item_bg"
                                style="background-image: url({{ isset($project->image) ? $project->image->thumb(305, 522) : '' }})"
                                >
                                <p class="cases__item-date">{{ $project->date->formatLocalized('%B %d') }}</p>
                                <p class="cases__item-title">
                                    <span class="cases__item-title-part">{{ $lang ? $project->title_ru : $project->title }}</span>
                                    <span class="cases__item-title-part">
                                        @if ($lang)
                                            {{ $project->clients->implode('title_ru', ', ') }}
                                        @else
                                            {{ $project->clients->implode('title', ', ') }}
                                        @endif
                                    </span>
                                </p>
                            </a>
                        </div>

                        @isset ($service->projects[1])
                            @php
                                $project = $service->projects[1];
                            @endphp
                            <div class="cases__column">
                                <a href="{{ action('PublicController@project', ['name' => $project->name, 'lang' => $lang]) }}" class="cases__item">
                                    <div class="cases__item-title">
                                        <p class="cases__item-title-part">{{ $lang ? $project->title_ru : $project->title }}</p>
                                        <p class="cases__item-title-part">
                                            @if ($lang)
                                                {{ $project->clients->implode('title_ru', ', ') }}
                                            @else
                                                {{ $project->clients->implode('title', ', ') }}
                                            @endif
                                        </p>
                                    </div>
                                    <div class="cases__item-image" style="background-image: url({{ isset($project->image) ? $project->image->thumb(240) : '' }})">
                                        <p class="cases__item-date">{{ $project->date->formatLocalized('%B %d') }}</p>
                                    </div>
                                </a>
                            </div>
                        @endisset

                        @isset ($service->projects[2])
                            @php
                                $project = $service->projects[2];
                            @endphp
                            <div class="cases__column">
                                <a href="{{ action('PublicController@project', ['name' => $project->name, 'lang' => $lang]) }}" class="cases__item">
                                    <div class="cases__item-image" style="background-image: url({{ isset($project->image) ? $project->image->thumb(480, 340) : '' }})">
                                        <p class="cases__item-date">{{ $project->date->formatLocalized('%B %d') }}</p>
                                    </div>
                                    <div class="cases__item-title">
                                        <p class="cases__item-title-part">{{ $lang ? $project->title_ru : $project->title }}</p>
                                        <p class="cases__item-title-part">
                                            @if ($lang)
                                                {{ $project->clients->implode('title_ru', ', ') }}
                                            @else
                                                {{ $project->clients->implode('title', ', ') }}
                                            @endif
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endisset
                    </div>
                </div>
            @endif
        </div>

        <div class="back-module portfolio-item__back">
            <a href="{{ action('PublicController@services', ['lang' => $lang]) }}" class="back-module__link">
                @if ($lang)
                    НАЗАД К УСЛУГАМ
                @else
                    BACK TO SERVICES
                @endif
            </a>
        </div>
    </div>
</div>

@endsection