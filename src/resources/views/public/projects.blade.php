@extends('layouts.public')

@section('title', ($lang ? "{$page->seo_title_ru} | " : "{$page->seo_title} | "))

@section('description', $page->description)

@section('content')

<div class="portfolio">
    <div class="portfolio__container middle-container">
        <h1 class="portfolio__title page-title">{{ $lang ? $page->title_ru : $page->title }}</h1>

        @isset($projects)
            <div class="portfolio__list">
                @foreach($projects as $project)
                    <a href="{{ action('PublicController@project', ['name' => $project->name, 'lang' => $lang]) }}" class="portfolio__item portfolio__item_photo">
                        <img src="img/1x1.png" data-src="{{ isset($project->image) ? $project->image->thumb(540, 340) : '' }}" alt="{{ $project->title }}" class="portfolio__item-img lazyload">
                        <p class="portfolio__item-date">@include('public.elements.date', ['date' => $project->date])</p>
                        <div class="portfolio__item-title">
                            <p class="portfolio__item-title-part">{{ $lang ? $project->title_ru : $project->title }}</p>
                            <p class="portfolio__item-title-part">
                                @if ($lang)
                                    {{ $project->clients->implode('title_ru', ', ') }}
                                @else
                                    {{ $project->clients->implode('title', ', ') }}
                                @endif
                            </p>
                        </div>
                    </a>
                @endforeach
            </div>
        @endisset

    </div>
</div>

@endsection
