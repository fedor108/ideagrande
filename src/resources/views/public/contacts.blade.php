@extends('layouts.public')

@section('title', ($lang ? "{$page->seo_title_ru} | " : "{$page->seo_title} | "))

@section('description', $lang ? $page->description_ru : $page->description)

@section('content')

<div class="contacts middle-container">
    <h2 class="contacts__title page-title">{{ $lang ? $page->title_ru : $page->title }}</h2>

    <div class="contacts__container">

        <div class="contacts__form-col js-contacts-form-col">

            <div class="contacts__form-col-wrap" id="contacts-form-wrap">
                <h3 class="contacts__form-col-title">
                    <div class="contacts__form-col-title-detail js-detail"></div>
                    <span class="contacts__form-col-title-text">{{ $lang ? 'Отправить сообщение' : 'Send us a message' }}</span>
                </h3>
                <div class="contacts__form form">
                    <form action="" id="contacts-form" name="contacts-form">
                        {{ csrf_field() }}
                        <div class="form__input-container">
                            <div class="form__input-holder js-input-holder" data-error="{{ $lang ? 'введите ваше имя' : 'enter your name' }}">
                                <label class="form__input-label">{{ $lang ? 'Имя' : 'Name' }}</label>
                                <input name="cname"
                                       type="text"
                                       class="form__input js-input"
                                       required
                                       placeholder="{{ $lang ? 'введите ваше имя' : 'enter your name' }}">
                            </div>
                            <div class="form__input-holder js-input-holder" data-error="{{ $lang ? 'введите название компании' : 'enter your company name' }}">
                                <label class="form__input-label">{{ $lang ? 'Компания' : 'Company' }}</label>
                                <input name="ccompany"
                                       type="text"
                                       class="form__input js-input"
                                       placeholder="{{ $lang ? 'введите название компании' : 'enter your company name' }}">
                            </div>
                        </div>

                        <div class="form__input-container">
                            <div class="form__input-holder js-input-holder" data-error="{{ $lang ? 'введите имейл' : 'enter your email' }}">
                                <label class="form__input-label">{{ $lang ? 'Имейл' : 'Email' }}</label>
                                <input name="cemail"
                                       type="email"
                                       class="form__input js-input"
                                       required
                                       placeholder="example@mail.ru">
                            </div>
                        </div>

                        <div class="form__input-holder js-input-holder" data-error="{{ $lang ? 'введите сообщение' : 'enter your comment' }}">
                            <label class="form__input-label">{{ $lang ? 'Сообщение' : 'Comment' }}</label>
                            <textarea name="ccomment"
                                      class="form__textarea js-input"
                                      required
                                      placeholder="{{ $lang ? 'введите сообщение' : 'enter your comment' }}"></textarea>
                        </div>

                        <div class="form__checkbox-holder">
                            <div class="form__checkbox">
                                <input name="cagree" type="checkbox" class="js-checkbox" id="cagree">
                            </div>
                            <div class="form__checkbox-text">
                                @include('public.elements.policy-checkbox-text')
                            </div>
                        </div>

                        <button class="btn js-button" type="submit" id="csubmit">
                            <div class="btn__bg"></div>
                            <span class="btn__text">{{ $lang ? 'Отправить' : 'Send' }}</span>
                        </button>

                    </form>
                </div>
            </div>

            <div class="contacts__form-success" id="contacts-form-message">
                <div class="contacts__form-success-detail js-detail"></div>
                <p class="contacts__form-success-text">{{ $lang ? 'Сообщение успешно отправлено' : 'Your message was successfully sent' }}</p>
                <a href="#" class="contacts__form-success-link" id="contacts-form-message-link">{{ $lang ? 'Отправить еще сообщение' : 'Send another message' }}</a>
            </div>

        </div>

        <div class="contacts__map-col">
            <h3 class="contacts__map-col-title">{{ $lang ? 'Адрес' : 'Address' }}</h3>

            <div class="contacts__map">
                <div class="contacts__address-block">
                    <div class="contacts__socials">
                        @isset ($settings['social'])
                            @foreach ($settings['social']->obj as $key => $url)
                                @if (empty($url))
                                    @continue
                                @endif
                                <a href="{{ $url }}" target="_blank" class="contacts__socials-item contacts__socials-item_{{ $key }}"></a>
                            @endforeach
                        @endisset
                    </div>
                    <div class="contacts__address">
                        {!! $lang ? nl2br($page->text_ru) : nl2br($page->text) !!}
                    </div>
                </div>
                <div id="google-map" class="contacts__google-map"></div>
            </div>
        </div>

    </div>

</div>

@endsection

@section('pagescripts')

<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('google-map'), {
            center: {lat: 55.768157, lng: 37.621817},
            zoom: 12,
            disableDefaultUI: true,
            scrollwheel: false,
            styles: [
                {
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#bdbdbd"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#eeeeee"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e5e5e5"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#dadada"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e5e5e5"
                        }
                    ]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#eeeeee"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#c9c9c9"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#bcddff"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                }
            ]
        });

        var marker = new google.maps.Marker({
            map: map,
            position: {lat: 55.768157, lng: 37.621817},
            icon: {
                url: '/img/icon-marker.svg',
                size: new google.maps.Size(40, 50),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 50)
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSlmb0kjYItIMJ-TUO4oagK1GySSLvSFI&callback=initMap"
        async defer></script>

@append