<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>@yield('title'){{ $lang ? $settings['title_ru']->value : $settings['title']->value }}</title>

    <meta name="description" content="@yield('description')">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#30d5c8">
    <meta name="theme-color" content="#ffffff">
</head>

<body>

<div class="all-wrap" id="all-wrap">

    @include('public.elements.header')

    @yield('content')

    @include('public.elements.footer')

    @include('public.elements.mobile-menu')

    @include('public.elements.cookies')

</div>

<link rel="stylesheet" href="/css/main.css?1506417877">
<script src="/scripts/app.min.js?1506417877"></script>

@yield('pagescripts')

</body>
</html>
