<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetPagesTitleRuNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->string('title_ru')->nullable()->change();
        });

        Schema::table('services', function (Blueprint $table) {
            $table->string('title_ru')->nullable()->change();
        });

        Schema::table('subservices', function (Blueprint $table) {
            $table->string('title_ru')->nullable()->change();
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->string('title_ru')->nullable()->change();
        });

        Schema::table('practices', function (Blueprint $table) {
            $table->string('title_ru')->nullable()->change();
        });

        Schema::table('persons', function (Blueprint $table) {
            $table->string('title_ru')->nullable()->change();
        });

        Schema::table('images', function (Blueprint $table) {
            $table->string('title_ru')->nullable()->change();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->string('title_ru')->change();
        });

        Schema::table('services', function (Blueprint $table) {
            $table->string('title_ru')->change();
        });

        Schema::table('subservices', function (Blueprint $table) {
            $table->string('title_ru')->change();
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->string('title_ru')->change();
        });

        Schema::table('practices', function (Blueprint $table) {
            $table->string('title_ru')->change();
        });

        Schema::table('persons', function (Blueprint $table) {
            $table->string('title_ru')->change();
        });

        Schema::table('images', function (Blueprint $table) {
            $table->string('title_ru')->change();
        });
    }
}
