<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('title');
            $table->string('title_ru')->nullable();
            $table->text('description')->nullable();
            $table->text('description_ru')->nullable();
            $table->timestamp('date');
            $table->text('task')->nullable();
            $table->text('task_ru')->nullable();
            $table->text('decision')->nullable();
            $table->text('decision_ru')->nullable();
            $table->text('result')->nullable();
            $table->text('result_ru')->nullable();
            $table->text('lead')->nullable();
            $table->text('lead_ru')->nullable();
            $table->string('video')->nullable();
            $table->text('video_ru')->nullable();
            $table->text('link')->nullable();
            $table->integer('people')->nullable();
            $table->integer('coverage')->nullable();
            $table->boolean('active')->nullable();
            $table->integer('sort')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
