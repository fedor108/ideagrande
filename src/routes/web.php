<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', function () {
    return redirect('/');
});

Route::get('/admin', 'AdminController@index');


Auth::routes();
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::get('/{lang?}', 'PublicController@home');
Route::get('/about/{lang?}', 'PublicController@about');
Route::get('/contacts/{lang?}', 'PublicController@contacts');
Route::get('/services/{lang?}', 'PublicController@services');
Route::get('/services/{name}/{lang?}', 'PublicController@service');
Route::get('/practices/{lang?}', 'PublicController@practices');
Route::get('/hiring/{lang?}', 'PublicController@vacancies');
Route::get('/cases/{lang?}', 'PublicController@projects');
Route::get('/cases/{name}/{lang?}', 'PublicController@project');
Route::post('/mails/send/report', 'MailController@sendReport');
Route::post('/mails/send/hiring', 'MailController@sendHiring');
Route::get('/news/{lang?}', 'PublicController@news');
Route::get('/news/{name}/{lang?}', 'PublicController@newsItem');
Route::get('/{name}/{lang?}', 'PublicController@page');


Route::get('/admin/services', 'AdminController@services');
Route::get('/admin/services/{id}', 'AdminController@service');

Route::get('/admin/subservices/{id}', 'AdminController@subservice');

Route::get('/admin/clients', 'AdminController@clients');
Route::get('/admin/clients/{id}', 'AdminController@client');

Route::get('/admin/practices', 'AdminController@practices');
Route::get('/admin/practices/{id}', 'AdminController@practice');

Route::get('/admin/persons', 'AdminController@persons');
Route::get('/admin/persons/{id}', 'AdminController@person');

Route::get('/admin/pages', 'AdminController@pages');
Route::get('/admin/pages/{id}', 'AdminController@page');

Route::get('/admin/images/{id}', 'AdminController@image');
Route::get('/admin/imageable/{id}', 'AdminController@imageable');

Route::get('/admin/settings/{id}', 'AdminController@setting');
Route::get('/admin/settings', 'AdminController@settings');

Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/users/{id}', 'AdminController@user');

Route::get('/admin/projects', 'AdminController@projects');
Route::get('/admin/projects/{id}', 'AdminController@project');

Route::get('/admin/vacancies', 'AdminController@vacancies');
Route::get('/admin/vacancies/{id}', 'AdminController@vacancy');

Route::get('/admin/events', 'AdminController@events');
Route::get('/admin/events/{id}', 'AdminController@event');

Route::get('/admin/news', 'AdminController@news');
Route::get('/admin/news/{id}', 'AdminController@newsItem');

Route::get('/admin/tags', 'AdminController@tags');
Route::get('/admin/tags/{id}', 'AdminController@tag');

Route::get('/admin/instagram', 'AdminController@instagram');


Route::post('/api/services/sort', 'ServiceController@sort');
Route::resource('/api/services', 'ServiceController');

Route::post('/api/subservices/sort', 'SubserviceController@sort');
Route::resource('/api/subservices', 'SubserviceController');

Route::post('/api/clients/sort', 'ClientController@sort');
Route::resource('/api/clients', 'ClientController');

Route::post('/api/practices/sort', 'PracticeController@sort');
Route::resource('/api/practices', 'PracticeController');

Route::post('/api/vacancies/sort', 'VacancyController@sort');
Route::resource('/api/vacancies', 'VacancyController');

Route::post('/api/persons/sort', 'PersonController@sort');
Route::resource('/api/persons', 'PersonController');

Route::post('/api/pages/sort', 'PageController@sort');
Route::resource('/api/pages', 'PageController');

Route::post('/api/images/sort', 'ImageController@sort');
Route::resource('/api/images', 'ImageController');

Route::resource('/api/settings', 'SettingController');
Route::resource('/api/users', 'UserController');

Route::delete('/api/clientables/delete', 'ClientableController@delete');
Route::resource('/api/clientables', 'ClientableController');

Route::delete('/api/personables/delete', 'PersonableController@delete');
Route::resource('/api/personables', 'PersonableController');

Route::post('/api/projects/sort', 'ProjectController@sort');
Route::resource('/api/projects', 'ProjectController');

Route::delete('/api/serviceables/delete', 'ServiceableController@delete');
Route::resource('/api/serviceables', 'ServiceableController');

Route::post('/api/events/sort', 'EventController@sort');
Route::resource('/api/events', 'EventController');

Route::resource('/api/news', 'NewsController');

Route::resource('/api/tags', 'TagController');

Route::delete('/api/tagables/delete', 'TagableController@delete');
Route::resource('/api/tagables', 'TagableController');

Route::post('/api/instagram/login', 'InstagramController@login');
Route::get('/api/instagram/success', 'InstagramController@success');
